package rs.ac.uns.ftn.siit.scientificcenterapi.service.process;

import org.camunda.bpm.engine.IdentityService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.siit.scientificcenterapi.dto.AuthorDTO;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.Author;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.ScientificWork;
import rs.ac.uns.ftn.siit.scientificcenterapi.service.application.AuthorService;
import rs.ac.uns.ftn.siit.scientificcenterapi.service.application.NotificationService;
import rs.ac.uns.ftn.siit.scientificcenterapi.service.application.ScientificWorkService;

import java.util.Optional;

@Service
public class TopicInvalidService implements JavaDelegate {

    @Autowired
    IdentityService identityService;

    @Autowired
    ScientificWorkService scientificWorkService;

    @Autowired
    NotificationService notificationService;

    @Autowired
    AuthorService authorService;

    @Override
    public void execute(DelegateExecution execution) throws Exception {

        System.out.println("Entered Topic Invalid Service Task");

        Long id = (Long) execution.getVariable("swId");

        Optional<ScientificWork> scientificWork = scientificWorkService.findById(id);

        if (scientificWork.isPresent()){
            scientificWork.get().setStatus("rejected");
            scientificWorkService.save(scientificWork.get());

            AuthorDTO authorDTO = (AuthorDTO) execution.getVariable("author");
            Optional<Author> authorMain = authorService.findByUsername(authorDTO.getUsername());

            String header =  "Poštovani/a " + authorMain.get().getJournalUser().getFirstName() + " " +
                    authorMain.get().getJournalUser().getLastName() + ", <br/><br/>";

            String body = header + "Vaš naučni rad pod nazivom <b><i>" + scientificWork.get().getTitle() +
                    "</i></b> je odbijen iz tematskih razloga";

            if (authorMain.isPresent()){
                notificationService.sendMessage(
                        authorMain.get().getJournalUser().getEmail(),
                        "Naučni rad je odbijen",
                        body);
            }
        }

    }
}
