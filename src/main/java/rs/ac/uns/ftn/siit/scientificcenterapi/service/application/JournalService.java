package rs.ac.uns.ftn.siit.scientificcenterapi.service.application;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.Journal;
import rs.ac.uns.ftn.siit.scientificcenterapi.repository.JournalRepository;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Service
public class JournalService {

    @Autowired
    private JournalRepository journalRepository;

    public Journal save(Journal journal){
        return journalRepository.save(journal);
    }

    public Optional<Journal> findById(Long id) {
        return journalRepository.findById(id);
    }

    public void deleteById(Long id){
        journalRepository.deleteById(id);
    }

    public Collection<Journal> findAll(){
        return journalRepository.findAll();
    }

    public List<Journal> getAllByPage(Pageable pageable) {
        Page<Journal> page = journalRepository.findAll(pageable);
        return page.getContent();
    }

    public Optional<Journal> findByISSN(String issn){
        return journalRepository.findByIssn(issn);
    }

}
