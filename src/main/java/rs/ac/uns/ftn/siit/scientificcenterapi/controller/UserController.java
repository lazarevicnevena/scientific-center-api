package rs.ac.uns.ftn.siit.scientificcenterapi.controller;

import org.camunda.bpm.engine.*;
import org.camunda.bpm.engine.form.FormField;
import org.camunda.bpm.engine.form.TaskFormData;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.camunda.bpm.engine.task.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import rs.ac.uns.ftn.siit.scientificcenterapi.handler.HelperFunctions;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.*;
import rs.ac.uns.ftn.siit.scientificcenterapi.service.application.AuthorService;
import rs.ac.uns.ftn.siit.scientificcenterapi.service.application.EditorService;
import rs.ac.uns.ftn.siit.scientificcenterapi.service.application.JournalUserService;
import rs.ac.uns.ftn.siit.scientificcenterapi.service.application.ReviewerService;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping(value = "/users")
public class UserController {

    @Autowired
    IdentityService identityService;

    @Autowired
    private RuntimeService runtimeService;

    @Autowired
    private RepositoryService repositoryService;

    @Autowired
    TaskService taskService;

    @Autowired
    FormService formService;

    @Autowired
    AuthorService authorService;

    @Autowired
    EditorService editorService;

    @Autowired
    ReviewerService reviewerService;

    @Autowired
    JournalUserService journalUserService;

    @RequestMapping(
            value = "/tasks/{taskId}",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<FormDetails> submitRegistrationForm(@RequestBody List<FormSubmission> objects, @PathVariable String taskId) {
        HashMap<String, Object> map = HelperFunctions.mapListToObject(objects);

        Task task = taskService.createTaskQuery().taskId(taskId).singleResult();
        String processInstanceId = task.getProcessInstanceId();
        runtimeService.setVariable(processInstanceId, "registration", objects);

        formService.submitTaskForm(taskId, map);

        // after calling service task submit same data for login user task
        Task task2 = taskService.createTaskQuery().processInstanceId(processInstanceId).list().get(0);
        String processInstanceIdLogin = task2.getProcessInstanceId();
        runtimeService.setVariable(processInstanceIdLogin, "login", objects);

        formService.submitTaskForm(task2.getId(), map);

        // after calling service task get form of next user task -> choosing journal
        Task task3 = taskService.createTaskQuery().processInstanceId(processInstanceIdLogin).list().get(0);
        TaskFormData taskFormData = formService.getTaskFormData(task3.getId());
        List<FormField> formFields = taskFormData.getFormFields();
        System.out.println("Next Task: " + task3.getTaskDefinitionKey());
        FormDetails formDetails = new FormDetails(task3.getId(), formFields, processInstanceIdLogin);

        return new ResponseEntity<>(formDetails, HttpStatus.OK);
    }

    @RequestMapping(
            value = "/{username}/role",
            method = RequestMethod.GET
    )
    public ResponseEntity<String> getRole(@PathVariable String username) {

        String role = "";
        Optional<Author> author = authorService.findByUsername(username);
        if (author.isPresent()){
            role = "author";
        }
        Optional<Reviewer> reviewer = reviewerService.findByUsername(username);
        if (reviewer.isPresent()){
            role = "reviewer";
        }

        Optional<Editor> editor = editorService.findByUsername(username);
        if (editor.isPresent()){
            role = "editor";
        }

        if (role.equals("")){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(role, HttpStatus.OK);
    }
}
