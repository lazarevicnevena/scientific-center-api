package rs.ac.uns.ftn.siit.scientificcenterapi.dto;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class TaskSWDto {
	
	String taskId;
	String processInstanceId;
	ScientificWorkDTO data;

}
