package rs.ac.uns.ftn.siit.scientificcenterapi.model;

import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
public class Journal {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String title;
    private String issn;
    private boolean openAccess;
    // from bank for paying
    private String sellerId;
    private String paymentOptions;
    private BigDecimal price;

    @OneToOne(fetch = FetchType.EAGER)
    private Editor editor;

}
