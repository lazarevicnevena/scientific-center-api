package rs.ac.uns.ftn.siit.scientificcenterapi.dto.indexing;

import lombok.*;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.Author;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class IndexingAuthorDTO {

    private String firstName;
    private String lastName;
    private String email;
    private String city;
    private String state;

    public IndexingAuthorDTO(Author author){
        this.firstName = author.getJournalUser().getFirstName();
        this.lastName = author.getJournalUser().getLastName();
        this.email = author.getJournalUser().getEmail();
        this.city = author.getJournalUser().getCity();
        this.state = author.getJournalUser().getState();
    }
}
