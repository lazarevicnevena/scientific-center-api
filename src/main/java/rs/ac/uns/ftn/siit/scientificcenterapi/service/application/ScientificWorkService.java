package rs.ac.uns.ftn.siit.scientificcenterapi.service.application;

import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.task.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import rs.ac.uns.ftn.siit.scientificcenterapi.dto.ScientificWorkDTO;
import rs.ac.uns.ftn.siit.scientificcenterapi.dto.TaskSWDto;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.Author;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.ScientificWork;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.ScientificWorkResult;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.SearchQuery;
import rs.ac.uns.ftn.siit.scientificcenterapi.repository.ScientificWorkRepository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Service
public class ScientificWorkService {

    @Autowired
    private ScientificWorkRepository scientificWorkRepository;

    @Autowired
    RuntimeService runtimeService;

    @Autowired
    AuthorService authorService;

    public ScientificWork save(ScientificWork scientificWork){
        return scientificWorkRepository.save(scientificWork);
    }

    public Optional<ScientificWork> findById(Long id) {
        return scientificWorkRepository.findById(id);
    }

    public void deleteById(Long id){
        scientificWorkRepository.deleteById(id);
    }

    public Collection<ScientificWork> findAll(){
        return scientificWorkRepository.findAll();
    }

    public ArrayList<ScientificWork> findByAuthors(ArrayList<Author> authors) {
        return scientificWorkRepository.findByAuthors(authors);
    }

    public ArrayList<ScientificWork> findByJournalIssn(String issn) {
        return scientificWorkRepository.findByJournal_Issn(issn);
    }

    public ArrayList<ScientificWork> findByStatus(String status) {
        return scientificWorkRepository.findByStatus(status);
    }

    public List<ScientificWork> getAllByPage(Pageable pageable) {
        Page<ScientificWork> page = scientificWorkRepository.findAll(pageable);
        return page.getContent();
    }

    public List<ScientificWork> getAllByPageAndStatus(Pageable pageable, String status) {
        Page<ScientificWork> page = scientificWorkRepository.findAllByStatus(pageable, status);
        return page.getContent();
    }

    public List<ScientificWork> getAllByPageByJournal(Pageable pageable, Long id) {
        Page<ScientificWork> page = scientificWorkRepository.findAllByJournal_Id(pageable, id);
        return page.getContent();
    }

    public List<ScientificWorkResult> searchAcceptedWorks(List<SearchQuery> queries){

        List<ScientificWorkResult> scientificWorkResults = new ArrayList<>();

        RestTemplate rest = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<List<SearchQuery>> entity = new HttpEntity<>(queries, headers);

        String url = "http://localhost:8450/api/searching";

        try {
            scientificWorkResults = rest.postForObject(url, entity, List.class);

            if (scientificWorkResults.isEmpty()){
                System.out.println(scientificWorkResults.toString());
            }

        }
        catch (Exception e){
            System.out.println("Result is none!");
        }

        return scientificWorkResults;
    }

    public List<TaskSWDto> getWorks( List<Task> tasks, String status){
        List<TaskSWDto> taskDtos = new ArrayList<>();
        for (Task task : tasks) {
            task.getProcessInstanceId();
            try{
                ScientificWork scientificWork = (ScientificWork) runtimeService.getVariable(task.getProcessInstanceId(),"scientificWork");

                boolean flag = true;

                if (!scientificWork.getStatus().equals(status)) {
                    flag = false;
                }


                ScientificWorkDTO scientificWorkDTO = new ScientificWorkDTO(
                        scientificWork,
                        scientificWork.getJournal().getTitle(),
                        scientificWork.getAuthors()
                );
                System.out.println(scientificWorkDTO.toString());
                TaskSWDto t = new TaskSWDto(task.getId(), task.getProcessInstanceId(), scientificWorkDTO);

                System.out.println("Task id: " + t.getTaskId() +
                        " Proccess instance id: " + t.getProcessInstanceId());



                if (!taskDtos.isEmpty()){
                    for (TaskSWDto td: taskDtos){
                        if (    td.getData().getTitle().equals(t.getData().getTitle()) &&
                                td.getData().getJournalTitle().equals(t.getData().getJournalTitle()) &&
                                td.getData().getKeywords().equals(t.getData().getKeywords())){
                            flag = false;
                            break;
                        }
                    }
                }

                if (flag){
                    taskDtos.add(t);
                }

            }catch (Exception e){
                System.out.println("Cannot find scientific work");
            }

        }

        return taskDtos;
    }

    public List<TaskSWDto> getSubmittedWorks(List<Task> tasks){
        List<TaskSWDto> taskDtos = new ArrayList<>();
        for (Task task : tasks) {
            task.getProcessInstanceId();
            try{
                ScientificWork scientificWork = (ScientificWork) runtimeService.getVariable(task.getProcessInstanceId(),"scientificWork");

                boolean flag = true;

                if (scientificWork.getStatus().equals("rejected") || scientificWork.getStatus().equals("accepted")
                        || scientificWork.getStatus().equals("format-changing")){
                    flag = false;
                }

                ScientificWorkDTO scientificWorkDTO = new ScientificWorkDTO(
                        scientificWork,
                        scientificWork.getJournal().getTitle(),
                        scientificWork.getAuthors()
                );
                System.out.println(scientificWorkDTO.toString());
                TaskSWDto t = new TaskSWDto(task.getId(), task.getProcessInstanceId(), scientificWorkDTO);

                System.out.println("Task id: " + t.getTaskId() +
                        " Proccess instance id: " + t.getProcessInstanceId());



                if (!taskDtos.isEmpty()){
                    for (TaskSWDto td: taskDtos){
                        if (    td.getData().getTitle().equals(t.getData().getTitle()) &&
                                td.getData().getJournalTitle().equals(t.getData().getJournalTitle()) &&
                                td.getData().getKeywords().equals(t.getData().getKeywords())){
                            flag = false;
                            break;
                        }
                    }
                }

                if (flag){
                    taskDtos.add(t);
                }

            }catch (Exception e){
                System.out.println("Cannot find scientific work");
            }

        }
        return taskDtos;
    }

    public List<TaskSWDto> getAssignedWorks(List<Task> tasks, String status1, String status2){
        List<TaskSWDto> taskDtos = new ArrayList<>();
        for (Task task : tasks) {
            task.getProcessInstanceId();
            try{
                ScientificWork scientificWork = (ScientificWork) runtimeService.getVariable(task.getProcessInstanceId(),"scientificWork");

                boolean flag = true;

                if (!scientificWork.getStatus().equals(status1) &&
                        !scientificWork.getStatus().equals(status2)){
                    flag = false;
                }

                ScientificWorkDTO scientificWorkDTO = new ScientificWorkDTO(
                        scientificWork,
                        scientificWork.getJournal().getTitle(),
                        scientificWork.getAuthors()
                );
                System.out.println(scientificWorkDTO.toString());
                TaskSWDto t = new TaskSWDto(task.getId(), task.getProcessInstanceId(), scientificWorkDTO);

                System.out.println("Task id: " + t.getTaskId() +
                        " Proccess instance id: " + t.getProcessInstanceId());



                if (!taskDtos.isEmpty()){
                    for (TaskSWDto td: taskDtos){
                        if (    td.getData().getTitle().equals(t.getData().getTitle()) &&
                                td.getData().getJournalTitle().equals(t.getData().getJournalTitle()) &&
                                td.getData().getKeywords().equals(t.getData().getKeywords())){
                            flag = false;
                            break;
                        }
                    }
                }

                if (flag){
                    taskDtos.add(t);
                }

            }catch (Exception e){
                System.out.println("Cannot find scientific work");
            }

        }
        return taskDtos;
    }

}
