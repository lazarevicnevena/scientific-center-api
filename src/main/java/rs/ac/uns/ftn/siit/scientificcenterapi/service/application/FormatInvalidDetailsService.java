package rs.ac.uns.ftn.siit.scientificcenterapi.service.application;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.Author;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.FormatInvalidDetails;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.ScientificWork;
import rs.ac.uns.ftn.siit.scientificcenterapi.repository.AuthorRepository;
import rs.ac.uns.ftn.siit.scientificcenterapi.repository.FormatInvalidDetailsRepository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Service
public class FormatInvalidDetailsService {

    @Autowired
    private FormatInvalidDetailsRepository formatInvalidDetailsRepository;

    public FormatInvalidDetails save(FormatInvalidDetails formatInvalidDetails){
        return formatInvalidDetailsRepository.save(formatInvalidDetails);
    }

    public Optional<FormatInvalidDetails> findById(Long id) {
        return formatInvalidDetailsRepository.findById(id);
    }

    public void deleteById(Long id){
        formatInvalidDetailsRepository.deleteById(id);
    }

    public Collection<FormatInvalidDetails> findAll(){
        return formatInvalidDetailsRepository.findAll();
    }

    public Optional<FormatInvalidDetails> findByScientificWorkId(Long id){
        return formatInvalidDetailsRepository.findByScientificWork_Id(id);
    }

}
