package rs.ac.uns.ftn.siit.scientificcenterapi.dto;

import lombok.*;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.SubjectArea;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class SubjectAreaDTO {

    private String area;

    private EditorDTO areaEditor;

    public SubjectAreaDTO(SubjectArea subjectArea) {
        this.area = subjectArea.getArea();
        this.areaEditor = new EditorDTO(subjectArea.getAreaEditor());
    }

}
