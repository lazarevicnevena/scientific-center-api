package rs.ac.uns.ftn.siit.scientificcenterapi.dto;

import lombok.*;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.Reviewer;

import java.io.Serializable;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ReviewerDTO implements Serializable {

    private String username;
    private String firstName;
    private String lastName;
    private String email;
    private String city;
    private String state;
    private String rank;

    public ReviewerDTO(Reviewer reviewer){
        this.username = reviewer.getJournalUser().getUsername();
        this.firstName = reviewer.getJournalUser().getFirstName();
        this.lastName = reviewer.getJournalUser().getLastName();
        this.email = reviewer.getJournalUser().getEmail();
        this.city = reviewer.getJournalUser().getCity();
        this.state = reviewer.getJournalUser().getState();
        this.rank = reviewer.getRank();
    }
}
