package rs.ac.uns.ftn.siit.scientificcenterapi.controller;

import org.camunda.bpm.engine.*;
import org.camunda.bpm.engine.form.FormField;
import org.camunda.bpm.engine.form.TaskFormData;
import org.camunda.bpm.engine.task.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import rs.ac.uns.ftn.siit.scientificcenterapi.dto.AuthorDTO;
import rs.ac.uns.ftn.siit.scientificcenterapi.dto.JournalDTO;
import rs.ac.uns.ftn.siit.scientificcenterapi.handler.HelperFunctions;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.*;
import rs.ac.uns.ftn.siit.scientificcenterapi.service.application.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping(value = "/journals")
public class JournalController {

    @Autowired
    JournalService journalService;

    @Autowired
    IdentityService identityService;

    @Autowired
    private RuntimeService runtimeService;

    @Autowired
    private RepositoryService repositoryService;

    @Autowired
    TaskService taskService;

    @Autowired
    FormService formService;

    @Autowired
    EditorService editorService;

    @Autowired
    SubjectAreaService subjectAreaService;

    @Autowired
    ReviewerService reviewerService;

    @Autowired
    AuthorService authorService;

    @Autowired
    JournalUserService journalUserService;

    @RequestMapping(
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<ArrayList<JournalDTO>> getJournals(@RequestParam int pageNum,
                                                             @RequestParam int pageSize){

        if (pageSize > 100)
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

        Pageable pageable = PageRequest.of(pageNum, pageSize);
        List<Journal> journals = journalService.getAllByPage(pageable);

        HttpHeaders headers = new HttpHeaders();
        headers.set("total-count", String.valueOf(journalService.findAll().size()));

        ArrayList<JournalDTO> journalArrayList = new ArrayList<>();
        for (Journal j: journals){
            List<SubjectArea> subjectAreas = subjectAreaService.findByJournal(j.getId());
            JournalDTO journal = new JournalDTO(j,subjectAreas);
            journalArrayList.add(journal);
        }

        return new ResponseEntity<>(journalArrayList, headers, HttpStatus.OK);
    }

    @RequestMapping(
            value = "tasks/{taskId}",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<FormDetails> journalChosen(@RequestBody List<FormSubmission> objects, @PathVariable String taskId) {
        HashMap<String, Object> map = HelperFunctions.mapListToObject(objects);

        Task task = taskService.createTaskQuery().taskId(taskId).singleResult();
        String processInstanceId = task.getProcessInstanceId();
        String issn = objects.get(0).getFieldValue();
        Optional<Journal> journal = journalService.findByISSN(issn);
        JournalDTO journalDTO = new JournalDTO();
        journalDTO.setIssn(issn);
        journalDTO.setTitle(journal.get().getTitle());
        journalDTO.setOpenAccess(journal.get().isOpenAccess());
        journalDTO.setPrice(journal.get().getPrice());
        journalDTO.setSellerId(journal.get().getSellerId());

        runtimeService.setVariable(processInstanceId, "journal", journalDTO);

        formService.submitTaskForm(taskId, map);

        // after calling service task get form of next user task
        Task task2 = taskService.createTaskQuery().processInstanceId(processInstanceId).list().get(0);

        TaskFormData taskFormData = formService.getTaskFormData(task2.getId());
        List<FormField> formFields = taskFormData.getFormFields();

        System.out.println("Next Task: " + task2.getTaskDefinitionKey());
        FormDetails formDetails = new FormDetails(task2.getId(), formFields, task2.getProcessInstanceId());

        return new ResponseEntity<>(formDetails, HttpStatus.OK);
    }

    @RequestMapping(
            value = "/users/tasks/{taskId}",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<FormDetails> submitRegistrationForm(@RequestBody List<FormSubmission> objects, @PathVariable String taskId) {
        HashMap<String, Object> map = HelperFunctions.mapListToObject(objects);

        Task task = taskService.createTaskQuery().taskId(taskId).singleResult();
        String processInstanceId = task.getProcessInstanceId();
        runtimeService.setVariable(processInstanceId, "registrationJournal", objects);
        map.remove("subjectArea");
        map.put("subjectArea", "engineering");

        formService.submitTaskForm(taskId, map);


        // after calling service task get form of next user task -> choosing journal
        Task task2 = taskService.createTaskQuery().processInstanceId(processInstanceId).list().get(0);
        TaskFormData taskFormData = formService.getTaskFormData(task2.getId());
        List<FormField> formFields = taskFormData.getFormFields();
        System.out.println("Next Task: " + task2.getTaskDefinitionKey());
        FormDetails formDetails = new FormDetails(task2.getId(), formFields, task2.getProcessInstanceId());

        return new ResponseEntity<>(formDetails, HttpStatus.OK);
    }
}
