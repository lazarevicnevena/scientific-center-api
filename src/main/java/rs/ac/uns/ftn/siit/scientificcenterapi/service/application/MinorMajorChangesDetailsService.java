package rs.ac.uns.ftn.siit.scientificcenterapi.service.application;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.FormatInvalidDetails;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.MinorMajorChangesDetails;
import rs.ac.uns.ftn.siit.scientificcenterapi.repository.FormatInvalidDetailsRepository;
import rs.ac.uns.ftn.siit.scientificcenterapi.repository.MinorMajorChangesDetailsRepository;

import java.util.Collection;
import java.util.Optional;

@Service
public class MinorMajorChangesDetailsService {

    @Autowired
    private MinorMajorChangesDetailsRepository minorMajorChangesDetailsRepository;

    public MinorMajorChangesDetails save(MinorMajorChangesDetails details){
        return minorMajorChangesDetailsRepository.save(details);
    }

    public Optional<MinorMajorChangesDetails> findById(Long id) {
        return minorMajorChangesDetailsRepository.findById(id);
    }

    public void deleteById(Long id){
        minorMajorChangesDetailsRepository.deleteById(id);
    }

    public Collection<MinorMajorChangesDetails> findAll(){
        return minorMajorChangesDetailsRepository.findAll();
    }

    public Optional<MinorMajorChangesDetails> findByScientificWorkId(Long id){
        return minorMajorChangesDetailsRepository.findByScientificWork_Id(id);
    }

}
