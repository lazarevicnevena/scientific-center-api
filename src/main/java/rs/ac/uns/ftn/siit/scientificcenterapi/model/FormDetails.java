package rs.ac.uns.ftn.siit.scientificcenterapi.model;

import lombok.*;
import org.camunda.bpm.engine.form.FormField;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class FormDetails {
	String taskId;
	List<FormField> formFields;
	String processInstanceId;
	
}
