package rs.ac.uns.ftn.siit.scientificcenterapi.service.process;

import org.camunda.bpm.engine.IdentityService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.camunda.bpm.engine.identity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.siit.scientificcenterapi.dto.AuthorDTO;
import rs.ac.uns.ftn.siit.scientificcenterapi.dto.JournalDTO;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.*;
import rs.ac.uns.ftn.siit.scientificcenterapi.service.application.*;

import java.util.List;
import java.util.Optional;

@Service
public class RegistrationJournalService implements JavaDelegate{

    @Autowired
    JournalService journalService;

    @Autowired
    JournalUserService journalUserService;

    @Autowired
    AuthorService authorService;

    @Autowired
    EditorService editorService;

    @Autowired
    ReviewerService reviewerService;

    @Autowired
    SubjectAreaService subjectAreaService;
	
	@Override
	public void execute(DelegateExecution execution) throws Exception {
        System.out.println("Entered Registration for Journal Service Task!");

        List<FormSubmission> registration = (List<FormSubmission>) execution.getVariable("registrationJournal");
        System.out.println(registration);
        String username = (String) execution.getVariable("loggedInUser");
        JournalDTO journalDTO = (JournalDTO) execution.getVariable("journal");
        Optional<Journal> journal = journalService.findByISSN(journalDTO.getIssn());
        String fName = "";
        String lName = "";
        String email = "";
        String state = "";
        String city = "";
        String role = "";
        String rank = "";
        String areas = "";

        for (FormSubmission formField : registration) {
            if (formField.getFieldId().equals("firstName")) {
                fName = formField.getFieldValue();
            }
            if (formField.getFieldId().equals("lastName")) {
                lName = formField.getFieldValue();
            }
            if (formField.getFieldId().equals("email")) {
                email = formField.getFieldValue();
            }
            if (formField.getFieldId().equals("city")) {
                city = formField.getFieldValue();
            }
            if (formField.getFieldId().equals("state")) {
                state = formField.getFieldValue();
            }
            if (formField.getFieldId().equals("role")) {
                role = formField.getFieldValue();
            }
            if (formField.getFieldId().equals("rank")) {
                rank = formField.getFieldValue();
            }
            if (formField.getFieldId().equals("subjectArea")) {
                areas = formField.getFieldValue();
            }
        }

        JournalUser journalUser = new JournalUser();
        journalUser.setUsername(username);
        journalUser.setFirstName(fName);
        journalUser.setLastName(lName);
        journalUser.setEmail(email);
        journalUser.setCity(city);
        journalUser.setState(state);
        journalUser = journalUserService.save(journalUser);

        if (role.equals("author")){
            Author author = new Author();
            author.setJournalUser(journalUser);
            author.setJournal(journal.get());
            author = authorService.save(author);

            AuthorDTO authorDTO = (AuthorDTO)execution.getVariable("author");
            if (authorDTO == null){
                AuthorDTO authorDTO1 = new AuthorDTO(author.getJournalUser().getUsername(),
                        author.getJournalUser().getFirstName(),
                        author.getJournalUser().getLastName(),
                        author.getJournalUser().getEmail(),
                        author.getJournalUser().getCity(),
                        author.getJournalUser().getState());
                execution.setVariable("author", authorDTO1);
                System.out.println("Author is set: " + authorDTO1.toString());
            }

        } else if (role.equals("reviewer")){
            Reviewer reviewer = new Reviewer();
            reviewer.setJournalUser(journalUser);
            reviewer.setRank(rank);
            reviewer.getJournals().add(journal.get());
            reviewer.setSubjectAreas(areas);
            reviewerService.save(reviewer);
        }else {
            Editor editor = new Editor();
            editor.setJournalUser(journalUser);
            editor.setRank(rank);
            editor.setJournal(journal.get());
            editor = editorService.save(editor);
            String[] tokens = areas.split("\\|");


            for (String  token: tokens){
                SubjectArea subjectArea = new SubjectArea();
                if (journal.isPresent()){
                    subjectArea.setJournal(journal.get());
                    subjectArea.setArea(token);
                    subjectArea.setAreaEditor(editor);
                    subjectAreaService.save(subjectArea);
                }
            }

            // because editors are at same time also reviewers

            Reviewer reviewer = new Reviewer();
            reviewer.setJournalUser(journalUser);
            reviewer.setRank(rank);
            reviewer.getJournals().add(journal.get());
            reviewer.setSubjectAreas(areas);
            reviewerService.save(reviewer);
        }
        execution.removeVariable("registrationJournal");
	}
}
