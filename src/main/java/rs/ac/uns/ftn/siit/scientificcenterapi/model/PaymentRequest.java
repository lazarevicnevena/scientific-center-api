package rs.ac.uns.ftn.siit.scientificcenterapi.model;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.sql.Timestamp;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class PaymentRequest {

    private BigDecimal amount;
    private String sellerId;
    private String scientificCenter;
    private String productIdentification; // can be journal or one scientific work
    private String productName;
    private Boolean onlyScientificWork;
    private Boolean membership; // regular payment of membership
    private String customerName;
}
