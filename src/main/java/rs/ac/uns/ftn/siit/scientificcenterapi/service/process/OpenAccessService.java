package rs.ac.uns.ftn.siit.scientificcenterapi.service.process;

import org.camunda.bpm.engine.IdentityService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import rs.ac.uns.ftn.siit.scientificcenterapi.dto.JournalDTO;

public class OpenAccessService implements JavaDelegate {

    @Autowired
    IdentityService identityService;

    @Override
    public void execute(DelegateExecution execution) throws Exception {

        System.out.println("Entered Open Access Service Task");

        JournalDTO journalDTO = (JournalDTO) execution.getVariable("journal");

        execution.setVariable("openAccess", journalDTO.isOpenAccess());

    }
}
