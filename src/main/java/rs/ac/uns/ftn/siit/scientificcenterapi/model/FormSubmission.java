package rs.ac.uns.ftn.siit.scientificcenterapi.model;

import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class FormSubmission implements Serializable{

	String fieldId;
	String fieldValue;

}
