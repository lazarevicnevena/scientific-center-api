package rs.ac.uns.ftn.siit.scientificcenterapi.service.application;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.Author;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.ScientificWork;
import rs.ac.uns.ftn.siit.scientificcenterapi.repository.AuthorRepository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Service
public class AuthorService {

    @Autowired
    private AuthorRepository authorRepository;

    public Author save(Author author){
        return authorRepository.save(author);
    }

    public Optional<Author> findById(Long id) {
        return authorRepository.findById(id);
    }

    public void deleteById(Long id){
        authorRepository.deleteById(id);
    }

    public Collection<Author> findAll(){
        return authorRepository.findAll();
    }

    public ArrayList<Author> findByScientificWorks(List<ScientificWork> scientificWorks) {
        return authorRepository.findByScientificWorks(scientificWorks);
    }

    public ArrayList<Author> findByJournalId(Long id) {
        return authorRepository.findByJournal_Id(id);
    }

    public Optional<Author> findByUsername(String username){
        return authorRepository.findByJournalUser_Username(username);
    }

    public Optional<Author> findByEmail(String email){
        return authorRepository.findByJournalUser_Email(email);
    }
}
