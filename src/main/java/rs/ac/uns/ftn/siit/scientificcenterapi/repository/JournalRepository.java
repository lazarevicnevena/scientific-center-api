package rs.ac.uns.ftn.siit.scientificcenterapi.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.Journal;

import java.util.Optional;


public interface JournalRepository extends JpaRepository<Journal, Long> {
    Page<Journal> findAll(Pageable pageable);
    Optional<Journal> findByIssn(String issn);
}
