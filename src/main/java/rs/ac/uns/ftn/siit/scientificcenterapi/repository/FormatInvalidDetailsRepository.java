package rs.ac.uns.ftn.siit.scientificcenterapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.FormatInvalidDetails;

import java.util.Optional;


public interface FormatInvalidDetailsRepository extends JpaRepository<FormatInvalidDetails, Long> {
    Optional<FormatInvalidDetails> findByScientificWork_Id(Long id);

}
