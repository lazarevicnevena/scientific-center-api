package rs.ac.uns.ftn.siit.scientificcenterapi.service.process;

import org.camunda.bpm.engine.IdentityService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.siit.scientificcenterapi.dto.AuthorDTO;
import rs.ac.uns.ftn.siit.scientificcenterapi.dto.JournalDTO;
import rs.ac.uns.ftn.siit.scientificcenterapi.handler.HelperFunctions;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.*;
import rs.ac.uns.ftn.siit.scientificcenterapi.service.application.AuthorService;
import rs.ac.uns.ftn.siit.scientificcenterapi.service.application.EditorService;
import rs.ac.uns.ftn.siit.scientificcenterapi.service.application.JournalService;
import rs.ac.uns.ftn.siit.scientificcenterapi.service.application.ReviewerService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ChoosingJournalService implements JavaDelegate {

    @Autowired
    IdentityService identityService;

    @Autowired
    JournalService journalService;

    @Autowired
    AuthorService authorService;

    @Autowired
    EditorService editorService;

    @Autowired
    ReviewerService reviewerService;

    @Override
    public void execute(DelegateExecution execution) throws Exception {

        System.out.println("Entered Choosing Journal Service Task");

        String username = (String)execution.getVariable("loggedInUser");

        JournalDTO journalDTO = (JournalDTO) execution.getVariable("journal");

        Optional<Journal> journal = journalService.findByISSN(journalDTO.getIssn());

        if (journal.isPresent()){

            ArrayList<Journal> journalArrayList = new ArrayList<>();
            journalArrayList.add(journal.get());

            ArrayList<Reviewer> reviewers = reviewerService.findByJournals(journalArrayList);

            ArrayList<Editor> editors = editorService.findByJournalId(journal.get().getId());

            List<Author> authors = authorService.findByJournalId(journal.get().getId());

            ArrayList<JournalUser> journalUsers = HelperFunctions.extractJournalUserFromReviewers(reviewers);
            ArrayList<JournalUser> journalEditors = HelperFunctions.extractJournalUserFromEditors(editors);
            ArrayList<JournalUser> journalAuthors = HelperFunctions.extractJournalUserFromAuthors(authors);

            journalUsers.addAll(journalEditors);
            journalUsers.addAll(journalAuthors);

            System.out.println("Ukupno korisnika časopisa : " + journalUsers.size());


            Optional<Author> author = authorService.findByUsername(username);
            if (author.isPresent()){
                AuthorDTO authorDTO = new AuthorDTO(username,
                        author.get().getJournalUser().getFirstName(),
                        author.get().getJournalUser().getLastName(),
                        author.get().getJournalUser().getEmail(),
                        author.get().getJournalUser().getCity(),
                        author.get().getJournalUser().getState());

                execution.setVariable("author", authorDTO);

                System.out.println("Author is set: " + authorDTO.toString());
            }

            Boolean flag = false;
            for (JournalUser journalUser: journalUsers){
                System.out.println("Journal user " + journalUser.toString());
                if (journalUser.getUsername().equals(username)){
                    flag = true;
                    break;
                }
            }

            execution.setVariable("registeredToJournal", flag);
            execution.removeVariable("journalUsers");

        }
    }
}
