package rs.ac.uns.ftn.siit.scientificcenterapi.service.application;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.FormatInvalidDetails;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.Membership;
import rs.ac.uns.ftn.siit.scientificcenterapi.repository.FormatInvalidDetailsRepository;
import rs.ac.uns.ftn.siit.scientificcenterapi.repository.MembershipRepository;

import java.util.Collection;
import java.util.Optional;

@Service
public class JournalMembershipService {

    @Autowired
    private MembershipRepository membershipRepository;

    public Membership save(Membership membership){
        return membershipRepository.save(membership);
    }

    public Optional<Membership> findById(Long id) {
        return membershipRepository.findById(id);
    }

    public void deleteById(Long id){
        membershipRepository.deleteById(id);
    }

    public Collection<Membership> findAll(){
        return membershipRepository.findAll();
    }

    public Optional<Membership> findByJournalId(Long id){
        return membershipRepository.findByJournal_Id(id);
    }

    public Optional<Membership> findByAuthorUsername(String username){
        return membershipRepository.findByAuthor_JournalUser_Username(username);
    }

}
