package rs.ac.uns.ftn.siit.scientificcenterapi.dto;

import lombok.*;
import rs.ac.uns.ftn.siit.scientificcenterapi.handler.FileHandler;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.Author;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.ScientificWork;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ScientificWorkDTO implements Serializable {

    private Long id;
    private String title;
    private String keywords;
    private String shortAbstract;
    private String subjectArea;
    private List<AuthorDTO> authors = new ArrayList<>();
    private String journalTitle;
    private String location;
    private String status;

    public ScientificWorkDTO(ScientificWork scientificWork, String journalTitle, List<Author> authors){
        this.id = scientificWork.getId();
        this.title = scientificWork.getTitle();
        this.keywords = scientificWork.getKeywords();
        this.shortAbstract = scientificWork.getShortAbstract();
        this.subjectArea = scientificWork.getSubjectArea();
        this.journalTitle = journalTitle;
        for (Author a: authors) {
            this.authors.add(new AuthorDTO(a));
        }
        FileHandler handler = new FileHandler();
        location = handler.location(this.title, this.authors.get(0).getUsername());
        this.status = scientificWork.getStatus();
    }

}
