package rs.ac.uns.ftn.siit.scientificcenterapi.controller;

import com.sun.org.apache.xpath.internal.operations.Bool;
import org.camunda.bpm.engine.*;
import org.camunda.bpm.engine.form.FormField;
import org.camunda.bpm.engine.form.TaskFormData;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.camunda.bpm.engine.task.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import rs.ac.uns.ftn.siit.scientificcenterapi.handler.HelperFunctions;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.FormDetails;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.FormSubmission;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Controller
@RequestMapping(value = "/login")
public class LoginController {

    @Autowired
    IdentityService identityService;

    @Autowired
    private RuntimeService runtimeService;

    @Autowired
    private RepositoryService repositoryService;

    @Autowired
    TaskService taskService;

    @Autowired
    FormService formService;

    @RequestMapping(
            value = "/registered/{flag}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<FormDetails> proceedToLoginOrRegistration(@PathVariable Boolean flag){
        ProcessInstance processInstance = runtimeService.startProcessInstanceByKey("ProcessFlow");
        Task t = taskService.createTaskQuery().processInstanceId(processInstance.getId()).list().get(0);
        FormSubmission formSubmission = new FormSubmission("registered", flag.toString());
        runtimeService.setVariable(t.getProcessInstanceId(), "registrationNeeded", formSubmission);
        List<FormSubmission> list = new ArrayList<>();
        list.add(formSubmission);
        formService.submitTaskForm(t.getId(), HelperFunctions.mapListToObject(list));

        Task task = taskService.createTaskQuery().processInstanceId(processInstance.getId()).list().get(0);
        TaskFormData taskFormData = formService.getTaskFormData(task.getId());
        List<FormField> formFields = taskFormData.getFormFields();

        System.out.println("Next Task: " + task.getTaskDefinitionKey());
        FormDetails formDetails = new FormDetails(task.getId(), formFields, task.getProcessInstanceId());

        return new ResponseEntity<>(formDetails, HttpStatus.OK);
    }

    @RequestMapping(
            value = "tasks/{taskId}",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<FormDetails> submitLoginForm(@RequestBody List<FormSubmission> objects, @PathVariable String taskId) {
        HashMap<String, Object> map = HelperFunctions.mapListToObject(objects);

        Task task = taskService.createTaskQuery().taskId(taskId).singleResult();
        String processInstanceId = task.getProcessInstanceId();
        runtimeService.setVariable(processInstanceId, "login", objects);

        formService.submitTaskForm(taskId, map);

        // after calling service task get form of next user task
        Task task2 = taskService.createTaskQuery().processInstanceId(processInstanceId).list().get(0);

        TaskFormData taskFormData = formService.getTaskFormData(task2.getId());
        List<FormField> formFields = taskFormData.getFormFields();
        System.out.println("Next Task: " + task2.getTaskDefinitionKey());
        FormDetails formDetails = new FormDetails(task2.getId(), formFields, task2.getProcessInstanceId());

        return new ResponseEntity<>(formDetails, HttpStatus.OK);
    }

}
