package rs.ac.uns.ftn.siit.scientificcenterapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.ReviewDetails;

import java.util.ArrayList;
import java.util.Optional;


public interface ReviewDetailsRepository extends JpaRepository<ReviewDetails, Long> {
    ArrayList<ReviewDetails> findByScientificWork_Id(Long id);
    ArrayList<ReviewDetails> findByReviewer_JournalUser_Username(String username);
    ArrayList<ReviewDetails> findByRecommendation(String recommendation);
    ArrayList<ReviewDetails> findByScientificWork_IdAndStatus(Long id, String status);
    Optional<ReviewDetails> findByReviewer_JournalUser_UsernameAndScientificWork_Id(String username, Long id);

}
