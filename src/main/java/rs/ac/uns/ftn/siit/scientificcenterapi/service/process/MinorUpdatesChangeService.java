package rs.ac.uns.ftn.siit.scientificcenterapi.service.process;

import org.camunda.bpm.engine.IdentityService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.ScientificWork;
import rs.ac.uns.ftn.siit.scientificcenterapi.service.application.ScientificWorkService;

import java.util.Optional;

@Service
public class MinorUpdatesChangeService implements JavaDelegate {

    @Autowired
    ScientificWorkService scientificWorkService;

    @Override
    public void execute(DelegateExecution execution) throws Exception {

        System.out.println("Entered Minor Updates Change Service Task");

        Long hours = (Long) execution.getVariable("hours");

        String dueTime = "PT" + hours.toString() + "H";
        System.out.println("DUE HOURS: " + dueTime);

        execution.setVariable("dueTime", dueTime);

        Long id = (Long) execution.getVariable("swId");

        Optional<ScientificWork> scientificWork = scientificWorkService.findById(id);

        if (scientificWork.isPresent()){
            scientificWork.get().setStatus("minor-updates");
            scientificWorkService.save(scientificWork.get());
        }

        execution.removeVariable("hours");
    }
}
