package rs.ac.uns.ftn.siit.scientificcenterapi.model;


import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
public class Editor implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String rank;

    @OneToOne
    private Journal journal;

    @OneToOne
    private JournalUser journalUser;

    @Transient
    private String subjectAreas;

}
