package rs.ac.uns.ftn.siit.scientificcenterapi.service.process;

import org.camunda.bpm.engine.IdentityService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.siit.scientificcenterapi.dto.ReviewerDTO;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.ReviewDetails;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.Reviewer;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.ScientificWork;
import rs.ac.uns.ftn.siit.scientificcenterapi.service.application.ReviewDetailsService;
import rs.ac.uns.ftn.siit.scientificcenterapi.service.application.ReviewerService;
import rs.ac.uns.ftn.siit.scientificcenterapi.service.application.ScientificWorkService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@Service
public class SettingReviewersService implements JavaDelegate {

    @Autowired
    IdentityService identityService;

    @Autowired
    ScientificWorkService scientificWorkService;

    @Autowired
    ReviewDetailsService reviewDetailsService;

    @Autowired
    ReviewerService reviewerService;

    @Override
    public void execute(DelegateExecution execution) throws Exception {

        System.out.println("Entered Setting Reviewers Service");

        String option = (String) execution.getVariable("option");

        ScientificWork sw = (ScientificWork) execution.getVariable("scientificWork");

        Optional<ScientificWork> scientificWork = scientificWorkService.findById(sw.getId());

        if (option.equals("first")){

            // if additional revision is needed or first revision

            List<ReviewerDTO> reviewerDTOS = (List<ReviewerDTO>) execution.getVariable("reviewerDTO");


            for (ReviewerDTO rd: reviewerDTOS){
                ReviewDetails reviewDetails = new ReviewDetails();
                reviewDetails.setStatus("assigned");
                if (scientificWork.isPresent()){
                    reviewDetails.setScientificWork(scientificWork.get());
                }

                Optional<Reviewer> reviewer = reviewerService.findByUsername(rd.getUsername());
                if (reviewer.isPresent()){
                    reviewDetails.setReviewer(reviewer.get());
                }
                reviewDetailsService.save(reviewDetails);

                execution.setVariable("reviewers", reviewerDTOS);

            }
        } else if (option.equals("second")){

            // if author had major changes applied and work is sent to previous reviewers

            ArrayList<ReviewDetails> reviewDetails = reviewDetailsService.findBySWAndStatus(sw.getId(), "done");

            List<ReviewerDTO> reviewerDTOS = new ArrayList<>();

            for (ReviewDetails rd: reviewDetails){
                reviewerDTOS.add(new ReviewerDTO(rd.getReviewer()));
            }

            for (ReviewDetails rd: reviewDetails){
                Optional<ReviewDetails> r = reviewDetailsService.findById(rd.getId());
                r.get().setStatus("assigned");
                r.get().setCommentToEditor("");
                r.get().setCommentToAuthor("");
                reviewDetailsService.save(r.get());
            }

            scientificWork.get().setStatus("revision-needed");
            scientificWorkService.save(scientificWork.get());

            execution.setVariable("reviewers", reviewerDTOS);
        }

    }
}
