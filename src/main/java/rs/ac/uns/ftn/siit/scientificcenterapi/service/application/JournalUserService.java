package rs.ac.uns.ftn.siit.scientificcenterapi.service.application;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.Editor;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.JournalUser;
import rs.ac.uns.ftn.siit.scientificcenterapi.repository.EditorRepository;
import rs.ac.uns.ftn.siit.scientificcenterapi.repository.JournalUserRepository;

import java.util.Collection;
import java.util.Optional;

@Service
public class JournalUserService {

    @Autowired
    private JournalUserRepository journalUserRepository;

    public JournalUser save(JournalUser journalUser){
        return journalUserRepository.save(journalUser);
    }

    public Optional<JournalUser> findById(Long id) {
        return journalUserRepository.findById(id);
    }

    public void deleteById(Long id){
        journalUserRepository.deleteById(id);
    }

    public Collection<JournalUser> findAll(){
        return journalUserRepository.findAll();
    }

    public Optional<JournalUser> findByUsername(String username) {
        return journalUserRepository.findByUsername(username);
    }
}
