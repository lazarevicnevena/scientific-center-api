package rs.ac.uns.ftn.siit.scientificcenterapi.service.process;

import com.sun.org.apache.xpath.internal.operations.Bool;
import org.camunda.bpm.engine.IdentityService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.camunda.bpm.engine.identity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.FormSubmission;

import java.util.List;

@Service
public class RegistrationUserService implements JavaDelegate{

	@Autowired
	IdentityService identityService;
	
	@Override
	public void execute(DelegateExecution execution) throws Exception {

		System.out.println("Entered Registration User Service Task");

		List<FormSubmission> registration = (List<FormSubmission>)execution.getVariable("registration");
		System.out.println(registration);
		String uname = "";
		String pwd = "";
		for (FormSubmission formField : registration) {
			if(formField.getFieldId().equals("username")) {
				uname = formField.getFieldValue();
			}
			if(formField.getFieldId().equals("password")) {
				pwd = formField.getFieldValue();
			}
		}

		// check if username is taken

        Boolean flag = false;
        User u = identityService.createUserQuery().userId(uname).singleResult();
        if (u == null){
            User user = identityService.newUser("");
            user.setId(uname);
            user.setPassword(pwd);
            identityService.saveUser(user);
            flag = true;
        }

        execution.setVariable("registrationDone", flag);
		execution.removeVariable("registration");

	}
}
