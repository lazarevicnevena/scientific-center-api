package rs.ac.uns.ftn.siit.scientificcenterapi.model;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
public class Reviewer implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String rank;
    private String subjectAreas;

    @ManyToMany
    private List<Journal> journals = new ArrayList<>();

    @OneToOne
    private JournalUser journalUser;

}
