package rs.ac.uns.ftn.siit.scientificcenterapi.service.process;

import org.camunda.bpm.engine.IdentityService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;
import rs.ac.uns.ftn.siit.scientificcenterapi.dto.AuthorDTO;
import rs.ac.uns.ftn.siit.scientificcenterapi.dto.JournalDTO;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.PaymentRequest;

public class PaymentService implements JavaDelegate {

    @Autowired
    IdentityService identityService;

    @Override
    public void execute(DelegateExecution execution) throws Exception {

        System.out.println("Entered Payment Service Task");

        JournalDTO journalDTO = (JournalDTO)execution.getVariable("journal");

        RestTemplate rest = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        PaymentRequest paymentRequest = new PaymentRequest();
        paymentRequest.setAmount(journalDTO.getPrice());
        paymentRequest.setSellerId(journalDTO.getSellerId());
        paymentRequest.setOnlyScientificWork(false);
        paymentRequest.setMembership(true);
        paymentRequest.setProductIdentification(journalDTO.getIssn());
        paymentRequest.setProductName(journalDTO.getTitle());
        paymentRequest.setScientificCenter("Scientific center - Nevena");
        AuthorDTO authorDTO = (AuthorDTO) execution.getVariable("author");
        if (authorDTO != null){
            paymentRequest.setCustomerName(authorDTO.getFirstName() + " " + authorDTO.getLastName());
        }

        System.out.println("Payment Request: " + paymentRequest.toString());

        HttpEntity<PaymentRequest> entity = new HttpEntity<>(paymentRequest, headers);

        String url = "https://localhost:8446/api/checkouts/token";

        // String response = rest.postForObject(url, entity, String.class);

        // TODO - Send real request and get information if it is successful or not!
        execution.setVariable("paymentSuccessful", true);

    }
}
