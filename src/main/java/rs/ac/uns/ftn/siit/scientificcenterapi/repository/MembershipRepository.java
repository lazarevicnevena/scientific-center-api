package rs.ac.uns.ftn.siit.scientificcenterapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.FormatInvalidDetails;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.Membership;

import java.util.Optional;


public interface MembershipRepository extends JpaRepository<Membership, Long> {
    Optional<Membership> findByJournal_Id(Long id);
    Optional<Membership> findByAuthor_JournalUser_Username(String username);

}
