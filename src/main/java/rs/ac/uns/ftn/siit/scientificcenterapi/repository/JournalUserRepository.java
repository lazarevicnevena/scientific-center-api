package rs.ac.uns.ftn.siit.scientificcenterapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.JournalUser;

import java.util.Optional;


public interface JournalUserRepository extends JpaRepository<JournalUser, Long> {
    Optional<JournalUser> findByUsername(String username);

}
