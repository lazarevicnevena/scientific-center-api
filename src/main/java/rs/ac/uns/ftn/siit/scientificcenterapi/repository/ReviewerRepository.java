package rs.ac.uns.ftn.siit.scientificcenterapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.Journal;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.Reviewer;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


public interface ReviewerRepository extends JpaRepository<Reviewer, Long> {
    ArrayList<Reviewer> findByJournals(List<Journal> journals);
    Optional<Reviewer> findByJournalUser_Username(String username);
    Optional<Reviewer> findByJournalUser_Email(String email);
    ArrayList<Reviewer> findByJournalsAndSubjectAreasContains(List<Journal> journals, String area);
}
