package rs.ac.uns.ftn.siit.scientificcenterapi.model;

import lombok.*;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
public class ScientificWork {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String title;
    private String keywords;
    private String shortAbstract;
    private String subjectArea;
    private String status;
    private String doi;

    @ManyToMany
    private List<Author> authors = new ArrayList<>();

    @Transient
    private MultipartFile pdf;

    @ManyToOne
    private Journal journal;


}
