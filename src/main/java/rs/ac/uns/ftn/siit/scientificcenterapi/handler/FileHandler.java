package rs.ac.uns.ftn.siit.scientificcenterapi.handler;

import lombok.NoArgsConstructor;
import org.apache.commons.fileupload.disk.DiskFileItem;
import org.apache.commons.io.IOUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

@NoArgsConstructor
public class FileHandler {


    public File getFilePath(String path) {
        Class k = this.getClass();
        ClassLoader l = k.getClassLoader();
        URL url = this.getClass().getClassLoader().getResource(path);
        System.out.println("class: "+ k.getName());
        System.out.println("URL: "+ url.toString());
        File file = null;
        try {
            file = new File(url.toURI());
        } catch (URISyntaxException e) {
            file = new File(url.getPath());
        }
        return file;
    }

    public String location(String title, String author) {
        String location = "";
        String path = getFilePath("files").getAbsolutePath() + File.separator + author + File.separator + title + ".pdf";
        if (new File(path).exists()){
            location = path;
        }
        return location;
    }

    public String saveFile(MultipartFile file, String authorUname, String title) throws IOException {
        String retVal = null;
        if (! file.isEmpty()) {
            byte[] bytes = file.getBytes();
            String base = getFilePath("files").getAbsolutePath();
            String pathDirectory = base + File.separator + authorUname;
            String pathFull = pathDirectory + File.separator + title + ".pdf";
            new File(pathDirectory).mkdirs();
            Path path = Paths.get(pathFull);
            Files.write(path, bytes);
            retVal = path.toString();
        }
        return retVal;
    }

    public MultipartFile getMultiPartFile(String title, String author){
        String location = location(title, author);
        File file = new File(location);
        DiskFileItem fileItem = new DiskFileItem("file", "text/plain", false, file.getName(), (int) file.length() , file.getParentFile());
        try {
            fileItem.getOutputStream();
        } catch (IOException e) {
            e.printStackTrace();
        }
        MultipartFile multipartFile = new CommonsMultipartFile(fileItem);
        return multipartFile;
    }

}
