package rs.ac.uns.ftn.siit.scientificcenterapi.dto;

import lombok.*;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.Editor;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class EditorDTO {

    private String firstName;
    private String lastName;
    private String email;
    private String city;
    private String state;
    private String rank;

    public EditorDTO(Editor editor){
        this.firstName = editor.getJournalUser().getFirstName();
        this.lastName = editor.getJournalUser().getLastName();
        this.email = editor.getJournalUser().getEmail();
        this.city = editor.getJournalUser().getCity();
        this.state = editor.getJournalUser().getState();
        this.rank = editor.getRank();
    }

}
