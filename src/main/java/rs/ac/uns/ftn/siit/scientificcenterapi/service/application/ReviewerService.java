package rs.ac.uns.ftn.siit.scientificcenterapi.service.application;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import rs.ac.uns.ftn.siit.scientificcenterapi.dto.ReviewerDTO;
import rs.ac.uns.ftn.siit.scientificcenterapi.dto.indexing.IndexingAuthorDTO;
import rs.ac.uns.ftn.siit.scientificcenterapi.handler.FileHandler;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.*;
import rs.ac.uns.ftn.siit.scientificcenterapi.repository.ReviewerRepository;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.*;

@Service
public class ReviewerService {

    @Autowired
    private ReviewerRepository reviewerRepository;

    @Autowired
    private AuthorService authorService;

    public Reviewer save(Reviewer reviewer){
        return reviewerRepository.save(reviewer);
    }

    public Optional<Reviewer> findById(Long id) {
        return reviewerRepository.findById(id);
    }

    public void deleteById(Long id){
        reviewerRepository.deleteById(id);
    }

    public Collection<Reviewer> findAll(){
        return reviewerRepository.findAll();
    }

    public ArrayList<Reviewer> findByJournals(ArrayList<Journal> journals){
        return reviewerRepository.findByJournals(journals);
    }

    public Optional<Reviewer> findByUsername(String username){
        return reviewerRepository.findByJournalUser_Username(username);
    }

    public Optional<Reviewer> findByEmail(String email){
        return reviewerRepository.findByJournalUser_Email(email);
    }

    public List<ReviewerDTO> getReviewers(String option, List<ReviewerDTO> previous,
                                          Journal journal, ScientificWork scientificWork,
                                          String chosenEditor){
        List<ReviewerDTO> reviewers = new ArrayList<>();

        boolean noprevious = true;
        HashMap<String, ReviewerDTO> map = new HashMap<>();
        if (previous != null || !previous.isEmpty()){
            noprevious = false;
            map = getHashmap(previous);
        }
        List<Reviewer> all = new ArrayList<>();
        List<ReviewerDTO> allDto = new ArrayList<>();
        switch (option) {
            case "all":
                all = reviewerRepository.findByJournals(Arrays.asList(journal));
                break;
            case "subject-area":
                all = reviewerRepository.findByJournalsAndSubjectAreasContains(Arrays.asList(journal), scientificWork.getSubjectArea());
                break;
            case "geo-distance":
                allDto = geoDistance(scientificWork);
                break;
            case "more-like-this":
                try {
                    allDto = moreLikeThis(scientificWork);
                } catch (IOException e) {
                    System.out.println("Could not get file.");
                }
                break;
        }

        if (option.equals("all") || option.equals("subject-area")){
            for (Reviewer reviewer: all){
                if (noprevious && !reviewer.getJournalUser().getUsername().equals(chosenEditor)){
                    reviewers.add(new ReviewerDTO(reviewer));
                }else {
                    if (!map.containsKey(reviewer.getJournalUser().getUsername()) &&
                            !reviewer.getJournalUser().getUsername().equals(chosenEditor)){
                        reviewers.add(new ReviewerDTO(reviewer));
                    }
                }
            }
        }else {
            for (ReviewerDTO reviewer: allDto){
                if (noprevious && !reviewer.getUsername().equals(chosenEditor)){
                    reviewers.add(reviewer);
                }else {
                    if (!map.containsKey(reviewer.getUsername()) && !reviewer.getUsername().equals(chosenEditor)){
                        reviewers.add(reviewer);
                    }
                }
            }
        }


        return reviewers;
    }

    public HashMap<String, ReviewerDTO> getHashmap(List<ReviewerDTO> previous){
        HashMap<String, ReviewerDTO> map = new HashMap<>();
        for (ReviewerDTO r: previous){
            map.put(r.getUsername(), r);
        }
        return map;
    }

    public List<ReviewerDTO> geoDistance(ScientificWork scientificWork){

        List<ReviewerDTO> response = new ArrayList<>();
        RestTemplate rest = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        List<Author> authors = scientificWork.getAuthors();
        List<IndexingAuthorDTO> authorDTOS = new ArrayList<>();
        for (Author a: authors){
            authorDTOS.add(new IndexingAuthorDTO(a));
        }

        GeoDistanceQuery geoDistanceQuery = new GeoDistanceQuery();
        geoDistanceQuery.setDistance(100);
        geoDistanceQuery.setAuthors(authorDTOS);

        HttpEntity<GeoDistanceQuery> entity = new HttpEntity<>(geoDistanceQuery ,headers);

        String url = "http://localhost:8450/api/searching/geo-distance";


        System.out.println("URL TO BE SENT: " + url);

        System.out.println(geoDistanceQuery.toString());

        try {
            ReviewerDTO[] objects = rest.postForObject(url, entity, ReviewerDTO[].class);

            response = Arrays.asList(objects);

            System.out.println(response);

        }
        catch (Exception e){
            System.out.println("Could not get any reviewers!");
        }

        return  response;
    }

    public List<ReviewerDTO> moreLikeThis(ScientificWork scientificWork) throws IOException {

        List<ReviewerDTO> response = new ArrayList<>();
        RestTemplate rest = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);

        String url = "http://localhost:8450/api/searching/more-like-this";

        FileHandler fileHandler = new FileHandler();

        String location = fileHandler.location(scientificWork.getTitle(), scientificWork.getAuthors().get(0).getJournalUser().getUsername());
        File f1 = new File(location);

        byte[] bytes = Files.readAllBytes(f1.toPath());


        ByteArrayResource contentsAsResource = new ByteArrayResource(bytes) {
            @Override
            public String getFilename() {
                // Filename has to be returned in order to be able to post.
                return location;
            }
        };


        MultiValueMap<String, Object> param = new LinkedMultiValueMap<>();
        param.add("file", contentsAsResource);

        HttpEntity<MultiValueMap<String, Object>> entity = new HttpEntity<>(param ,headers);

        System.out.println("URL TO BE SENT: " + url);


        try {
            ReviewerDTO[] objects = rest.postForObject(url, entity, ReviewerDTO[].class);

            response = Arrays.asList(objects);

            System.out.println(response.toString());

        }
        catch (Exception e){
            System.out.println("Could not get any reviewers!");
        }

        return  response;
    }

}
