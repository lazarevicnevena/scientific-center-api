package rs.ac.uns.ftn.siit.scientificcenterapi.model;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
public class MinorMajorChangesDetails implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String answeringComments;

    @OneToOne
    private ScientificWork scientificWork;

}
