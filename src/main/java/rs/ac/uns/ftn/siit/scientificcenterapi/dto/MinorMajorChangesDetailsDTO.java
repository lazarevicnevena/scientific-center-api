package rs.ac.uns.ftn.siit.scientificcenterapi.dto;

import lombok.*;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.Author;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.ScientificWork;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class MinorMajorChangesDetailsDTO implements Serializable {

    private Long id;

    private String answeringComments;

    private ScientificWorkDTO scientificWork;

    public MinorMajorChangesDetailsDTO(String answeringComments, ScientificWork scientificWork, List<Author> authors){
        this.answeringComments = answeringComments;
        this.scientificWork = new ScientificWorkDTO(scientificWork, "", authors);
    }

}
