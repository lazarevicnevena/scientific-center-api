package rs.ac.uns.ftn.siit.scientificcenterapi.service.process;

import org.camunda.bpm.engine.IdentityService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import rs.ac.uns.ftn.siit.scientificcenterapi.dto.AuthorDTO;
import rs.ac.uns.ftn.siit.scientificcenterapi.dto.ReviewerDTO;
import rs.ac.uns.ftn.siit.scientificcenterapi.dto.ScientificWorkDTO;
import rs.ac.uns.ftn.siit.scientificcenterapi.dto.indexing.IndexingAuthorDTO;
import rs.ac.uns.ftn.siit.scientificcenterapi.dto.indexing.MetaDataInfoDTO;
import rs.ac.uns.ftn.siit.scientificcenterapi.dto.indexing.UploadModelDTO;
import rs.ac.uns.ftn.siit.scientificcenterapi.handler.FileHandler;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.Author;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.ReviewDetails;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.ScientificWork;
import rs.ac.uns.ftn.siit.scientificcenterapi.service.application.AuthorService;
import rs.ac.uns.ftn.siit.scientificcenterapi.service.application.NotificationService;
import rs.ac.uns.ftn.siit.scientificcenterapi.service.application.ReviewDetailsService;
import rs.ac.uns.ftn.siit.scientificcenterapi.service.application.ScientificWorkService;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Optional;

@Service
public class WorkAcceptedService implements JavaDelegate {

    @Autowired
    ScientificWorkService scientificWorkService;

    @Autowired
    AuthorService authorService;

    @Autowired
    NotificationService notificationService;

    @Autowired
    ReviewDetailsService reviewDetailsService;

    @Override
    public void execute(DelegateExecution execution) throws Exception {

        System.out.println("Entered Work Accepted Service Task");

        Long id = (Long) execution.getVariable("swId");

        Optional<ScientificWork> scientificWork = scientificWorkService.findById(id);

        long secondNumber = (long) Math.floor(Math.random() * 9_000_000_000L) + 1_000_000_000L;
        long firstNumber = (long) Math.floor(Math.random() * 9000) + 1000;

        String doi = "10." + String.valueOf(firstNumber) + "/" + String.valueOf(secondNumber);

        System.out.println("DOI : " + doi);

        AuthorDTO authorDTO = (AuthorDTO) execution.getVariable("author");
        Optional<Author> authorOption = authorService.findByUsername(authorDTO.getUsername());

        if (scientificWork.isPresent()) {
            scientificWork.get().setStatus("accepted");
            scientificWork.get().setDoi(doi);
            ScientificWork sw = scientificWorkService.save(scientificWork.get());

            if (authorOption.isPresent()){
                String header =  "Poštovani/a " + authorOption.get().getJournalUser().getFirstName() + " " +
                        authorOption.get().getJournalUser().getLastName() + ", <br/><br/>";

                String body = header + "Vaš naučni rad pod nazivom <b><i>" + scientificWork.get().getTitle() +
                        "</i></b> je prihvaćen, te će biti objavljen u časopisu!";

                notificationService.sendMessage(
                        authorOption.get().getJournalUser().getEmail(),
                        "Naučni rad prihvaćen",
                        body);
            }

            // communication with searching gateway for indexing

            FileHandler fileHandler = new FileHandler();
            MultipartFile file = fileHandler.getMultiPartFile(sw.getTitle(), authorOption.get().getJournalUser().getUsername());
            MetaDataInfoDTO metaDataInfoDTO = new MetaDataInfoDTO();
            metaDataInfoDTO.setTitle(sw.getTitle());
            metaDataInfoDTO.setJournalTitle(sw.getJournal().getTitle());
            metaDataInfoDTO.setKeywords(sw.getKeywords());
            metaDataInfoDTO.setShortAbstract(sw.getShortAbstract());
            metaDataInfoDTO.setSubjectArea(sw.getSubjectArea());
            metaDataInfoDTO.setDoi(sw.getDoi());
            metaDataInfoDTO.setOpenAccess(sw.getJournal().isOpenAccess());
            for (Author a: sw.getAuthors()){
                metaDataInfoDTO.getAuthors().add(new IndexingAuthorDTO(a));
            }

            UploadModelDTO uploadModelDTO = new UploadModelDTO();
            uploadModelDTO.setFile(file);
            uploadModelDTO.setMetaDataInfo(metaDataInfoDTO);
            ArrayList<ReviewDetails> reviewDetails = reviewDetailsService.findBySWAndStatus(sw.getId(), "done");
            if (!reviewDetails.isEmpty()){
                for (ReviewDetails rd: reviewDetails){
                    uploadModelDTO.getReviewers().add(new ReviewerDTO(rd.getReviewer()));
                }
            }

            sendRequest(uploadModelDTO, authorOption.get().getJournalUser().getUsername());
        }

    }

    public void sendRequest(UploadModelDTO model, String authorUsername) throws IOException {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);

        RestTemplate rest = new RestTemplate();

        FileHandler handler = new FileHandler();
        String location = handler.location(model.getMetaDataInfo().getTitle(), authorUsername);
        File f1 = new File(location);

        byte[] bytes = Files.readAllBytes(f1.toPath());


        ByteArrayResource contentsAsResource = new ByteArrayResource(bytes) {
            @Override
            public String getFilename() {
                // Filename has to be returned in order to be able to post.
                return location;
            }
        };


        MultiValueMap<String, Object> param = new LinkedMultiValueMap<>();
        param.add("file", contentsAsResource);
        param.add("metaDataInfo", model.getMetaDataInfo());
        param.add("reviewers", model.getReviewers());

        HttpEntity<MultiValueMap<String, Object>> entity = new HttpEntity<>(param ,headers);

        String url = "http://localhost:8450/api/indexes";

        System.out.println("URL TO BE SENT: " + url);

        try {
            String response = rest.postForObject(url, entity, String.class);
            System.out.println("Response from Search gateway" + response);
        }
        catch (Exception e){
            System.out.println("Could not index file!");
            e.printStackTrace();
        }
    }
}
