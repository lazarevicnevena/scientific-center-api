package rs.ac.uns.ftn.siit.scientificcenterapi.model;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
public class ReviewDetails implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String commentToAuthor;
    private String commentToEditor;
    private String recommendation;

    private String status;

    @OneToOne
    private ScientificWork scientificWork;

    @OneToOne
    private Reviewer reviewer;

}
