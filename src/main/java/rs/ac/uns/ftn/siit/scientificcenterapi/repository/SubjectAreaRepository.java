package rs.ac.uns.ftn.siit.scientificcenterapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.Journal;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.SubjectArea;

import java.util.ArrayList;
import java.util.Optional;


public interface SubjectAreaRepository extends JpaRepository<SubjectArea, Long> {
    ArrayList<SubjectArea> findByJournal_Id(Long journalId);
    Optional<SubjectArea> findByJournal_IssnAndAndArea(String issn, String area);

}
