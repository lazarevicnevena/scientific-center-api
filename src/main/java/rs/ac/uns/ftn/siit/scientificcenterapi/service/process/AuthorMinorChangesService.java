package rs.ac.uns.ftn.siit.scientificcenterapi.service.process;

import org.camunda.bpm.engine.IdentityService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.FormSubmission;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.MinorMajorChangesDetails;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.ScientificWork;
import rs.ac.uns.ftn.siit.scientificcenterapi.service.application.MinorMajorChangesDetailsService;
import rs.ac.uns.ftn.siit.scientificcenterapi.service.application.ScientificWorkService;

import java.util.List;
import java.util.Optional;

@Service
public class AuthorMinorChangesService implements JavaDelegate {

    @Autowired
    IdentityService identityService;

    @Autowired
    MinorMajorChangesDetailsService minorMajorChangesDetailsService;

    @Autowired
    ScientificWorkService scientificWorkService;

    @Override
    public void execute(DelegateExecution execution) throws Exception {

        System.out.println("Entered Author Minor Changes Service Task");

        List<FormSubmission> objects = (List<FormSubmission>)execution.getVariable("authorChanges");
        String abstract_ = "";
        String keywords = "";
        String comments = "";
        for (FormSubmission formField : objects) {
            if(formField.getFieldId().equals("abstract")) {
                abstract_ = formField.getFieldValue();
            }
            if(formField.getFieldId().equals("keywords")) {
                keywords = formField.getFieldValue();
            }
            if(formField.getFieldId().equals("answeringComments")) {
                comments = formField.getFieldValue();
            }
        }

        Long id = (Long) execution.getVariable("swForChange");

        Optional<ScientificWork> scientificWork = scientificWorkService.findById(id);

        if (scientificWork.isPresent()){
            scientificWork.get().setKeywords(keywords);
            scientificWork.get().setShortAbstract(abstract_);
            scientificWork.get().setStatus("minor-updates-done");
            ScientificWork sw = scientificWorkService.save(scientificWork.get());

            Optional<MinorMajorChangesDetails> minorMajorChangesDetails =
                    minorMajorChangesDetailsService.findByScientificWorkId(sw.getId());
            if (minorMajorChangesDetails.isPresent()){
                minorMajorChangesDetails.get().setAnsweringComments(comments);
                minorMajorChangesDetailsService.save(minorMajorChangesDetails.get());
            } else {
                MinorMajorChangesDetails details = new MinorMajorChangesDetails();
                details.setScientificWork(sw);
                details.setAnsweringComments(comments);
                minorMajorChangesDetailsService.save(details);
            }

            System.out.println("Author changes minor changes!");
        }

        execution.removeVariable("authorChanges");
        execution.removeVariable("swForChange");
    }
}
