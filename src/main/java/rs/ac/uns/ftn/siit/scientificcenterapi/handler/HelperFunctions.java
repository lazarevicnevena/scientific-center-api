package rs.ac.uns.ftn.siit.scientificcenterapi.handler;

import org.springframework.beans.factory.annotation.Autowired;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class HelperFunctions {

    public static HashMap<String, Object> mapListToObject(List<FormSubmission> list)
    {
        HashMap<String, Object> map = new HashMap<>();
        for(FormSubmission temp : list){
            System.out.println("Polje: " + temp.getFieldId() + " vrijednost: " + temp.getFieldValue());
            map.put(temp.getFieldId(), temp.getFieldValue());
        }

        return map;
    }

    public static ArrayList<JournalUser> extractJournalUserFromReviewers(List<Reviewer> list)
    {
        ArrayList<JournalUser> arrayList = new ArrayList<>();
        for(Reviewer reviewer : list){
            arrayList.add(reviewer.getJournalUser());
        }

        return arrayList;
    }

    public static ArrayList<JournalUser> extractJournalUserFromEditors(List<Editor> list)
    {
        ArrayList<JournalUser> arrayList = new ArrayList<>();
        for(Editor editor : list){
            arrayList.add(editor.getJournalUser());
        }


        return arrayList;
    }

    public static ArrayList<JournalUser> extractJournalUserFromAuthors(List<Author> list)
    {
        ArrayList<JournalUser> arrayList = new ArrayList<>();
        for(Author author : list){
            arrayList.add(author.getJournalUser());
        }


        return arrayList;
    }
}
