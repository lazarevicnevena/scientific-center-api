package rs.ac.uns.ftn.siit.scientificcenterapi.dto;

import lombok.*;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.Author;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class AuthorDTO implements Serializable {

    private String username;
    private String firstName;
    private String lastName;
    private String email;
    private String city;
    private String state;

    public AuthorDTO(Author author){
        this.username = author.getJournalUser().getUsername();
        this.firstName = author.getJournalUser().getFirstName();
        this.lastName = author.getJournalUser().getLastName();
        this.email = author.getJournalUser().getEmail();
        this.city = author.getJournalUser().getCity();
        this.state = author.getJournalUser().getState();
    }
}
