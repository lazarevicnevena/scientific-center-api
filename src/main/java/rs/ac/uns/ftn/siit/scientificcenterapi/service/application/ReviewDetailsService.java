package rs.ac.uns.ftn.siit.scientificcenterapi.service.application;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.*;
import rs.ac.uns.ftn.siit.scientificcenterapi.repository.FormatInvalidDetailsRepository;
import rs.ac.uns.ftn.siit.scientificcenterapi.repository.ReviewDetailsRepository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Service
public class ReviewDetailsService {

    @Autowired
    private ReviewDetailsRepository reviewDetailsRepository;

    @Autowired
    private ScientificWorkService scientificWorkService;

    @Autowired
    private ReviewerService reviewerService;

    public ReviewDetails save(ReviewDetails reviewDetails){
        return reviewDetailsRepository.save(reviewDetails);
    }

    public Optional<ReviewDetails> findById(Long id) {
        return reviewDetailsRepository.findById(id);
    }

    public void deleteById(Long id){
        reviewDetailsRepository.deleteById(id);
    }

    public Collection<ReviewDetails> findAll(){
        return reviewDetailsRepository.findAll();
    }

    public ArrayList<ReviewDetails> findByScientificWorkId(Long id){
        return reviewDetailsRepository.findByScientificWork_Id(id);
    }

    public ArrayList<ReviewDetails> findByReviewerUsername(String username){
        return reviewDetailsRepository.findByReviewer_JournalUser_Username(username);
    }

    public ArrayList<ReviewDetails> findByRecommendation(String recommendation){
        return reviewDetailsRepository.findByRecommendation(recommendation);
    }

    public Optional<ReviewDetails> findBySWAndReviewer(Long id, String username){
        return reviewDetailsRepository.findByReviewer_JournalUser_UsernameAndScientificWork_Id(username, id);
    }

    public ArrayList<ReviewDetails> findBySWAndStatus(Long id, String status){
        return reviewDetailsRepository.findByScientificWork_IdAndStatus(id, status);
    }

    public void oneRevisionDone(String reviewerUname, Long id, List<FormSubmission> objects){

        String commentToAuthor = "";
        String recommendation = "";
        String commentToEditor = "";
        for (FormSubmission formField : objects) {
            if(formField.getFieldId().equals("commentToAuthor")) {
                commentToAuthor = formField.getFieldValue();
            }
            if(formField.getFieldId().equals("recommendation")) {
                recommendation = formField.getFieldValue();
            }
            if(formField.getFieldId().equals("commentToEditor")) {
                commentToEditor = formField.getFieldValue();
            }
        }
        Optional<Reviewer> reviewer = reviewerService.findByUsername(reviewerUname);

        Optional<ScientificWork> scientificWork = scientificWorkService.findById(id);

        Optional<ReviewDetails> reviewDetails = findBySWAndReviewer(id, reviewerUname);

        if (scientificWork.isPresent() && reviewer.isPresent() && reviewDetails.isPresent()){

            reviewDetails.get().setCommentToAuthor(commentToAuthor);
            reviewDetails.get().setCommentToEditor(commentToEditor);
            reviewDetails.get().setRecommendation(recommendation);
            reviewDetails.get().setStatus("done");

            save(reviewDetails.get());

            ArrayList<ReviewDetails> reviewDetailsArrayList = findBySWAndStatus(id, "done");
            ArrayList<ReviewDetails> reviewDetailsAssigned = findBySWAndStatus(id, "assigned");

            Integer minimum = 0;
            if (scientificWork.get().getStatus().equals("revision-needed")){
                minimum = 2;
            } else if (scientificWork.get().getStatus().equals("additional-revision-needed")) {
                minimum = 3;
            }

            System.out.println("Minimum revisions needed: " + minimum);

            if (reviewDetailsArrayList.size() >= minimum && reviewDetailsAssigned.isEmpty()) {
                scientificWork.get().setStatus("reviewed");
                scientificWorkService.save(scientificWork.get());
            }

        }
    }
}
