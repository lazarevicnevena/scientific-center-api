package rs.ac.uns.ftn.siit.scientificcenterapi.dto;

import lombok.*;
import rs.ac.uns.ftn.siit.scientificcenterapi.handler.FileHandler;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.Author;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.ReviewDetails;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.Reviewer;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.ScientificWork;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ReviewDetailsDTO implements Serializable {

    private Long id;

    private String commentToAuthor;
    private String commentToEditor;
    private String recommendation;

    private String status;

    private ReviewerDTO reviewer;

    public ReviewDetailsDTO(ReviewDetails reviewDetails, Reviewer reviewer){
        this.id = reviewDetails.getId();
        this.commentToAuthor = reviewDetails.getCommentToAuthor();
        this.recommendation = reviewDetails.getRecommendation();
        this.commentToEditor = reviewDetails.getCommentToEditor();
        this.status = reviewDetails.getStatus();
        this.reviewer = new ReviewerDTO(reviewer);
    }

}
