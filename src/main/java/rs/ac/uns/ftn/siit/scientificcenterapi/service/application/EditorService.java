package rs.ac.uns.ftn.siit.scientificcenterapi.service.application;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.Editor;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.Journal;
import rs.ac.uns.ftn.siit.scientificcenterapi.repository.EditorRepository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

@Service
public class EditorService {
    @Autowired
    private EditorRepository editorRepository;

    public Editor save(Editor editor){
        return editorRepository.save(editor);
    }

    public Optional<Editor> findById(Long id) {
        return editorRepository.findById(id);
    }

    public void deleteById(Long id){
        editorRepository.deleteById(id);
    }

    public Collection<Editor> findAll(){
        return editorRepository.findAll();
    }

    public ArrayList<Editor> findByJournalId(Long id){
        return editorRepository.findByJournal_Id(id);
    }

    public Optional<Editor> findByUsername(String username){
        return editorRepository.findByJournalUser_Username(username);
    }
}
