package rs.ac.uns.ftn.siit.scientificcenterapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.FormatInvalidDetails;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.MinorMajorChangesDetails;

import java.util.Optional;


public interface MinorMajorChangesDetailsRepository extends JpaRepository<MinorMajorChangesDetails, Long> {
    Optional<MinorMajorChangesDetails> findByScientificWork_Id(Long id);
}
