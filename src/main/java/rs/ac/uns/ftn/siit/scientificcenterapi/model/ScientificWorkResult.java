package rs.ac.uns.ftn.siit.scientificcenterapi.model;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ScientificWorkResult {

    private String title;
    private String journal;
    private String subjectArea;
    private String keywords;
    private String location;
    private Boolean openAccess;
    private String doi;
    private String authors;
    private String highlight;

}
