package rs.ac.uns.ftn.siit.scientificcenterapi.controller;

import com.sun.org.apache.xpath.internal.operations.Bool;
import org.camunda.bpm.engine.*;
import org.camunda.bpm.engine.form.FormField;
import org.camunda.bpm.engine.form.TaskFormData;
import org.camunda.bpm.engine.task.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import rs.ac.uns.ftn.siit.scientificcenterapi.dto.JournalDTO;
import rs.ac.uns.ftn.siit.scientificcenterapi.dto.ReviewerDTO;
import rs.ac.uns.ftn.siit.scientificcenterapi.dto.TaskSWDto;
import rs.ac.uns.ftn.siit.scientificcenterapi.handler.HelperFunctions;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.*;
import rs.ac.uns.ftn.siit.scientificcenterapi.service.application.JournalService;
import rs.ac.uns.ftn.siit.scientificcenterapi.service.application.ReviewDetailsService;
import rs.ac.uns.ftn.siit.scientificcenterapi.service.application.ReviewerService;
import rs.ac.uns.ftn.siit.scientificcenterapi.service.application.ScientificWorkService;

import java.util.*;

@Controller
@RequestMapping(value = "/reviewers")
public class ReviewerController {

    @Autowired
    IdentityService identityService;

    @Autowired
    private RuntimeService runtimeService;

    @Autowired
    private RepositoryService repositoryService;

    @Autowired
    TaskService taskService;

    @Autowired
    FormService formService;

    @Autowired
    ScientificWorkService scientificWorkService;

    @Autowired
    ReviewerService reviewerService;

    @Autowired
    JournalService journalService;

    @Autowired
    ReviewDetailsService reviewDetailsService;

    @RequestMapping(
            value = "/processes/{processInstanceId}/option/{option}/additional/{additional}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<List<ReviewerDTO>> getReviewers(@PathVariable String processInstanceId,
                                                          @PathVariable String option,
                                                          @PathVariable Boolean additional) {

        ScientificWork sw = (ScientificWork) runtimeService.getVariable(processInstanceId,"scientificWork");

        Optional<ScientificWork> scientificWork = scientificWorkService.findById(sw.getId());

        if (!scientificWork.isPresent()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        List<ReviewerDTO> reviewerDTOS = new ArrayList<>();
        List<ReviewDetails> details = reviewDetailsService.findByScientificWorkId(sw.getId());
        if (!details.isEmpty()){
            for (ReviewDetails rd: details){
                reviewerDTOS.add(new ReviewerDTO(rd.getReviewer()));
            }
        }

        JournalDTO journalDTO = (JournalDTO) runtimeService.getVariable(processInstanceId, "journal");

        Optional<Journal> journal = journalService.findByISSN(journalDTO.getIssn());
        if (!journal.isPresent()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        HttpHeaders headers = new HttpHeaders();

        Integer num = 0;

        if (reviewerDTOS.isEmpty() || additional == false){
            num = 2;
        } else {
            num = 1;
        }

        ArrayList<ReviewDetails> reviewDetailsTimedOut = reviewDetailsService.findBySWAndStatus(sw.getId(), "not-in-time");

        if (!reviewDetailsTimedOut.isEmpty() && reviewDetailsTimedOut.size() == 1) {
            num = 1;
        }

        System.out.println("Total needed " + num);

        headers.set("total-needed", num.toString());

        String chosenEditor = (String) runtimeService.getVariable(processInstanceId, "chosenEditor");

        List<ReviewerDTO> reviewers = reviewerService.getReviewers(
                option,
                reviewerDTOS,
                journal.get(),
                scientificWork.get(),
                chosenEditor);

        if (reviewers.isEmpty()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(reviewers, headers, HttpStatus.OK);
    }

    @RequestMapping(
            value = "/choose/tasks/{taskId}/processes/{processInstanceId}/deadline/{days}/option/{option}",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<String> setReviewers(@RequestBody(required = false) List<ReviewerDTO> reviewerDTOS,
                                               @PathVariable String taskId,
                                               @PathVariable String processInstanceId,
                                               @PathVariable Integer days,
                                               @PathVariable String option) {

        if (reviewerDTOS.isEmpty()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        runtimeService.setVariable(processInstanceId,"days", days);
        runtimeService.setVariable(processInstanceId,"option", option);
        if (option.equals("first")){
            runtimeService.setVariable(processInstanceId,"reviewerDTO", reviewerDTOS);
        }

        List<FormSubmission> formSubmissions = new ArrayList<>(Arrays.asList(
                    new FormSubmission("deadlineReviewers", "P"+days.toString()+"D")));


        HashMap<String, Object> map = HelperFunctions.mapListToObject(formSubmissions);
        String editor = (String) runtimeService.getVariable(processInstanceId, "chosenEditor");
        Task task = taskService.createTaskQuery().processInstanceId(processInstanceId).taskAssignee(editor).taskDefinitionKey("ChoosingReviewersTask").singleResult();
        formService.submitTaskForm(task.getId(), map);

        return new ResponseEntity<>("Everything is ok!", HttpStatus.OK);
    }

    @RequestMapping(
            value = "/choose/tasks/{taskId}/processes/{processInstanceId}/additional",
            method = RequestMethod.GET
    )
    public ResponseEntity<FormDetails> setAdditionalRevision(@PathVariable String taskId,
                                               @PathVariable String processInstanceId) {


        List<FormSubmission> formSubmissions = new ArrayList<>(Arrays.asList(
                    new FormSubmission("additionalRevision", String.valueOf(true))));

            runtimeService.setVariable(processInstanceId,"additionalRevision", true);

        String username = (String) runtimeService.getVariable(processInstanceId, "chosenEditor");

        HashMap<String, Object> map = HelperFunctions.mapListToObject(formSubmissions);
        Task task = taskService.createTaskQuery()
                .taskAssignee(username)
                .processInstanceId(processInstanceId)
                .taskDefinitionKey("EditorDecisionTask").singleResult();
        formService.submitTaskForm(task.getId(), map);

        // want to try to send back task and procc inst id of next task
        Task task2 = taskService.createTaskQuery().processInstanceId(processInstanceId).list().get(0);

        TaskFormData taskFormData = formService.getTaskFormData(task2.getId());
        List<FormField> formFields = taskFormData.getFormFields();

        System.out.println("Next Task: " + task2.getTaskDefinitionKey());
        FormDetails formDetails = new FormDetails(task2.getId(), formFields, task2.getProcessInstanceId());

        return new ResponseEntity<>(formDetails, HttpStatus.OK);
    }

    @RequestMapping(
            value = "/{reviewerUname}/scientific-works/assigned",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<List<TaskSWDto>> getAssignedWorks(@PathVariable String reviewerUname){

        Optional<Reviewer> reviewer = reviewerService.findByUsername(reviewerUname);

        if (!reviewer.isPresent()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        System.out.println("Reviewer from controller: " + reviewerUname);
        List<Task> tasks = taskService.createTaskQuery().taskAssignee(reviewerUname).taskDefinitionKey("ReviewerRevisionTask").list();
        List<TaskSWDto> taskDtos = scientificWorkService.getAssignedWorks(tasks, "revision-needed", "additional-revision-needed");


        return new ResponseEntity<>(taskDtos, HttpStatus.OK);
    }

    @RequestMapping(
            value = "/{reviewerUname}/revision-form-details",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<FormDetails> getRevisionFormFields(@PathVariable String reviewerUname){

        Task task = taskService.createTaskQuery().taskAssignee(reviewerUname).taskDefinitionKey("ReviewerRevisionTask").list().get(0);

        TaskFormData taskFormData = formService.getTaskFormData(task.getId());
        List<FormField> formFields = taskFormData.getFormFields();

        FormDetails formDetails = new FormDetails(task.getId(), formFields, task.getProcessInstanceId());

        return new ResponseEntity<>(formDetails, HttpStatus.OK);
    }

    @RequestMapping(
            value = "/{reviewerUname}/tasks/{taskId}/processes/{processInstanceId}/send-revision",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<String> submitRevisionForm(@RequestBody List<FormSubmission> objects,
                                                     @PathVariable String reviewerUname,
                                                     @PathVariable String taskId,
                                                     @PathVariable String processInstanceId) {

        List<FormSubmission> copy = processList(objects, "recommendation");

        HashMap<String, Object> map = HelperFunctions.mapListToObject(copy);

        formService.submitTaskForm(taskId, map);

        Long id = (Long) runtimeService.getVariable(processInstanceId,"swId");

        reviewDetailsService.oneRevisionDone(reviewerUname, id, copy);

        return new ResponseEntity<>("Everything is ok!", HttpStatus.OK);
    }

    @RequestMapping(
            value = "/{editorUname}/editor-decision-form-details",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<FormDetails> getEditorDecisionFormFields(@PathVariable String editorUname){

        Task task = taskService.createTaskQuery().taskAssignee(editorUname).taskDefinitionKey("EditorDecisionTask").list().get(0);

        TaskFormData taskFormData = formService.getTaskFormData(task.getId());
        List<FormField> formFields = taskFormData.getFormFields();

        FormDetails formDetails = new FormDetails(task.getId(), formFields, task.getProcessInstanceId());

        return new ResponseEntity<>(formDetails, HttpStatus.OK);
    }

    @RequestMapping(
            value = "/tasks/{taskId}/processes/{processInstanceId}/decision/{option}",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<String> submitEditorsDecision(@RequestBody List<FormSubmission> objects,
                                                        @PathVariable String taskId,
                                                        @PathVariable String processInstanceId,
                                                        @PathVariable String option) {

        List<FormSubmission> copy = processList(objects, "decision");

        HashMap<String, Object> map = HelperFunctions.mapListToObject(copy);

        Integer time = 0;

        String acceptedValue = "";

        for (FormSubmission fs: objects){
            if (option.equals("first")) {
                if (fs.getFieldId().equals("days")){
                    time = Integer.parseInt(fs.getFieldValue());
                    break;
                }
            } else if (option.equals("second")) {
                if (fs.getFieldId().equals("hours")){
                    time = Integer.parseInt(fs.getFieldValue());
                }
                if (fs.getFieldId().equals("accepted")){
                    acceptedValue = fs.getFieldValue();
                }
            }

        }

        if (option.equals("first")) {
            runtimeService.setVariable(processInstanceId, "days", time);
        } else {
            runtimeService.setVariable(processInstanceId, "hours", time);
            map.put("accepted", Boolean.parseBoolean(acceptedValue));
        }

        formService.submitTaskForm(taskId, map);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    public List<FormSubmission> processList(List<FormSubmission> objects, String name){
        for (FormSubmission formSubmission: objects){
            if (formSubmission.getFieldId().equals(name)){
                if (formSubmission.getFieldValue().equals("Prihvatiti")){
                    formSubmission.setFieldValue("accept");
                } else if (formSubmission.getFieldValue().equals("Prihvatiti uz manje prepravke")){
                    formSubmission.setFieldValue("minorUpdates");
                } else if (formSubmission.getFieldValue().equals("Uslovno prihvatiti uz veće prepravke")){
                    formSubmission.setFieldValue("majorUpdates");
                } else {
                    formSubmission.setFieldValue("reject");
                }
                break;
            }
        }
        return objects;
    }

    @RequestMapping(
            value = "/{authorUname}/author-changes-work",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<FormDetails> getAuthorChangesWorkFormFiels(@PathVariable String authorUname){

        Task task = taskService.createTaskQuery().taskAssignee(authorUname).taskDefinitionKey("ChangingWorkTask").list().get(0);

        TaskFormData taskFormData = formService.getTaskFormData(task.getId());
        List<FormField> formFields = taskFormData.getFormFields();

        FormDetails formDetails = new FormDetails(task.getId(), formFields, task.getProcessInstanceId());

        return new ResponseEntity<>(formDetails, HttpStatus.OK);
    }

}
