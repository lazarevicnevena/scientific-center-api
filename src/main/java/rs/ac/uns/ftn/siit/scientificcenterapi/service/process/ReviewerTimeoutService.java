package rs.ac.uns.ftn.siit.scientificcenterapi.service.process;

import org.camunda.bpm.engine.IdentityService;
import org.camunda.bpm.engine.TaskService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.camunda.bpm.engine.task.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.Editor;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.ReviewDetails;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.ScientificWork;
import rs.ac.uns.ftn.siit.scientificcenterapi.service.application.EditorService;
import rs.ac.uns.ftn.siit.scientificcenterapi.service.application.NotificationService;
import rs.ac.uns.ftn.siit.scientificcenterapi.service.application.ReviewDetailsService;
import rs.ac.uns.ftn.siit.scientificcenterapi.service.application.ScientificWorkService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ReviewerTimeoutService implements JavaDelegate {

    @Autowired
    IdentityService identityService;

    @Autowired
    ScientificWorkService scientificWorkService;

    @Autowired
    ReviewDetailsService reviewDetailsService;

    @Autowired
    TaskService taskService;

    @Autowired
    NotificationService notificationService;

    @Autowired
    EditorService editorService;

    @Override
    public void execute(DelegateExecution execution) throws Exception {

        System.out.println("Entered Reviewer Timeout Service Task");

        Long id = (Long) execution.getVariable("swId");

        Optional<ScientificWork> scientificWork = scientificWorkService.findById(id);

        String proccInstanceId = execution.getProcessInstanceId();

        if (scientificWork.isPresent()){
            ArrayList<ReviewDetails> reviewDetails = reviewDetailsService.findBySWAndStatus(id, "assigned");

            String reviewers = "";

            for (ReviewDetails rd: reviewDetails){
                Optional<ReviewDetails> r = reviewDetailsService.findById(rd.getId());
                r.get().setStatus("not-in-time");
                reviewDetailsService.save(r.get());

                reviewers += rd.getReviewer().getJournalUser().getFirstName() + " " +
                        rd.getReviewer().getJournalUser().getLastName() + ", ";

                Task task = taskService.createTaskQuery().taskAssignee(rd.getReviewer().getJournalUser().getUsername())
                        .taskDefinitionKey("ReviewerRevisionTask")
                        .processInstanceId(proccInstanceId)
                        .singleResult();
                taskService.complete(task.getId());

            }

            reviewers = reviewers.substring(0, reviewers.length()-2);

            String chosenEditor = (String) execution.getVariable("chosenEditor");

            Optional<Editor> editor = editorService.findByUsername(chosenEditor);

            if (editor.isPresent()){
                String header =  "Poštovani/a " + editor.get().getJournalUser().getFirstName() + " " +
                        editor.get().getJournalUser().getLastName() + ", <br/><br/>";

                String body = header + "Recenzenti: <i>" + reviewers + "</i> nisu uradili recenziju na vrijeme!" +
                        "<br/>Molimo Vas da odaberete druge recenzente.";

                notificationService.sendMessage(
                        editor.get().getJournalUser().getEmail(),
                        "Vrijeme za recenziranje isteklo",
                        body
                );
            }

        }

    }
}
