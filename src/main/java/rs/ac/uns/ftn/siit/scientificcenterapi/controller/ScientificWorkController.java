package rs.ac.uns.ftn.siit.scientificcenterapi.controller;

import org.camunda.bpm.engine.*;
import org.camunda.bpm.engine.form.FormField;
import org.camunda.bpm.engine.form.TaskFormData;
import org.camunda.bpm.engine.task.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.*;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import rs.ac.uns.ftn.siit.scientificcenterapi.dto.*;
import rs.ac.uns.ftn.siit.scientificcenterapi.handler.FileHandler;
import rs.ac.uns.ftn.siit.scientificcenterapi.handler.HelperFunctions;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.*;
import rs.ac.uns.ftn.siit.scientificcenterapi.service.application.*;

import java.util.*;

@Controller
@RequestMapping(value = "/scientific-works")
public class ScientificWorkController {

    @Autowired
    IdentityService identityService;

    @Autowired
    private RuntimeService runtimeService;

    @Autowired
    TaskService taskService;

    @Autowired
    FormService formService;

    @Autowired
    ScientificWorkService scientificWorkService;

    @Autowired
    AuthorService authorService;

    @Autowired
    JournalService journalService;

    @Autowired
    JournalUserService journalUserService;

    @Autowired
    EditorService editorService;

    @Autowired
    FormatInvalidDetailsService formatInvalidDetailsService;

    @Autowired
    ReviewDetailsService reviewDetailsService;

    @Autowired
    ReviewerService reviewerService;

    @Autowired
    MinorMajorChangesDetailsService minorMajorChangesDetailsService;

    @RequestMapping(
            value = "/journals/{issn}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<ArrayList<ScientificWorkDTO>> getScientificWorks( @RequestParam int pageNum,
                                                                            @RequestParam int pageSize,
                                                                            @PathVariable String issn){

        if (pageSize > 100)
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

        Pageable pageable = PageRequest.of(pageNum, pageSize);
        Optional<Journal> journal = journalService.findByISSN(issn);
        if (journal.isPresent()) {
            List<ScientificWork> scientificWorks = scientificWorkService.getAllByPageByJournal(pageable, journal.get().getId());
            String total = String.valueOf(scientificWorkService.findByJournalIssn(issn));

            HttpHeaders headers = new HttpHeaders();
            headers.set("total-count", total);

            ArrayList<ScientificWorkDTO> scientificWorkDTOS = new ArrayList<>();
            ArrayList<ScientificWork> list = new ArrayList<>();
            for (ScientificWork sw : scientificWorks) {
                list.add(sw);
                List<Author> authors = authorService.findByScientificWorks(list);
                ScientificWorkDTO scientificWorkDTO = new ScientificWorkDTO(sw, journal.get().getTitle(), authors);
                scientificWorkDTOS.add(scientificWorkDTO);
                list.clear();
            }

            return new ResponseEntity<>(scientificWorkDTOS, headers, HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(
            value = "/tasks/{taskId}",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<FormDetails> submitScientificWork(@RequestParam(value = "file", required = false) MultipartFile file,
                                                            @RequestParam("title") String title,
                                                            @RequestParam("keywords") String keywords,
                                                            @RequestParam("abstract") String abstract_,
                                                            @RequestParam("subjectArea") String subjectArea,
                                                            @RequestParam("authors") String authors,
                                                            @RequestParam(value = "id", required = false) String id,
                                                            @PathVariable String taskId) {

        ArrayList<FormSubmission> objects = new ArrayList<>();
        objects.add(new FormSubmission("title", title));
        objects.add(new FormSubmission("keywords", keywords));
        objects.add(new FormSubmission("abstract", abstract_));
        objects.add(new FormSubmission("subjectArea", subjectArea));
        objects.add(new FormSubmission("authors", authors));


        Task task = taskService.createTaskQuery().taskId(taskId).singleResult();
        String processInstanceId = task.getProcessInstanceId();
        runtimeService.setVariable(processInstanceId, "scientificWorkSubmission", objects);

        if (id != null){
            runtimeService.setVariable(processInstanceId, "swForChange", Long.parseLong(id));
        } else {
            runtimeService.setVariable(processInstanceId, "swForChange", 0L);
        }

        String username = (String) runtimeService.getVariable(processInstanceId, "loggedInUser");

        String path = saveFile(file, username, title);

        HashMap<String, Object> map = HelperFunctions.mapListToObject(objects);

        map.put("pdf", path);
        map.remove("subjectArea");
        map.put("subjectArea", "engineering");

        JournalDTO journalDTO = (JournalDTO) runtimeService.getVariable(processInstanceId, "journal");

        Optional<Journal> journal = journalService.findByISSN(journalDTO.getIssn());

        if (!journal.isPresent()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        runtimeService.setVariable(processInstanceId, "editor", journal.get().getEditor().getJournalUser().getUsername());

        formService.submitTaskForm(taskId, map);


        // after calling service task get form of next user task -> is scientific work valid
        Task task2 = taskService.createTaskQuery().processInstanceId(processInstanceId).list().get(0);
        TaskFormData taskFormData = formService.getTaskFormData(task2.getId());
        List<FormField> formFields = taskFormData.getFormFields();
        System.out.println("Next Task: " + task2.getTaskDefinitionKey());
        FormDetails formDetails = new FormDetails(task2.getId(), formFields, task2.getProcessInstanceId());


        return new ResponseEntity<>(formDetails, HttpStatus.OK);
    }



    @RequestMapping(
            value = "/submitted/editors/{editor}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<List<TaskSWDto>> getSubmittedWorks(@PathVariable String editor) {

        List<Task> tasks = taskService.createTaskQuery().taskAssignee(editor).taskDefinitionKey("ScientificWorkValidTask").list();
        List<TaskSWDto> taskDtos = scientificWorkService.getSubmittedWorks(tasks);

        return new ResponseEntity<>(taskDtos, HttpStatus.OK);

    }

    @RequestMapping(
            value = "/reviewed/editors/{editor}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<List<TaskSWDto>> getReviewedWorks(@PathVariable String editor) {

        List<Task> tasks = taskService.createTaskQuery().taskAssignee(editor).taskDefinitionKey("EditorDecisionTask").list();
        List<TaskSWDto> taskDtos = scientificWorkService.getWorks(tasks, "reviewed");

        return new ResponseEntity<>(taskDtos, HttpStatus.OK);

    }

    @RequestMapping(
            value = "/{id}/authors/{authorUname}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<ScientificWorkDTO> getScientificWork(@PathVariable Long id,
                                                               @PathVariable String authorUname) {

        Optional<ScientificWork> scientificWork = scientificWorkService.findById(id);
        if (!scientificWork.isPresent()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        Optional<Author> author = authorService.findByUsername(authorUname);

        ScientificWorkDTO scientificWorkDTO = new ScientificWorkDTO(scientificWork.get(), "", Arrays.asList(author.get()));

        return new ResponseEntity<>(scientificWorkDTO, HttpStatus.OK);

    }

    @RequestMapping(
            value = "/{id}/format-invalid-comment",
            method = RequestMethod.GET
    )
    public ResponseEntity<String> getFormatInvalidComment(@PathVariable Long id) {


        Optional<FormatInvalidDetails> formatInvalidDetails = formatInvalidDetailsService.findByScientificWorkId(id);

        if (!formatInvalidDetails.isPresent()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(formatInvalidDetails.get().getComment(), HttpStatus.OK);

    }

    @RequestMapping(
            value = "/{id}/revision-comments",
            method = RequestMethod.GET
    )
    public ResponseEntity<List<String>> getRevisionComments(@PathVariable Long id) {

        List<String> comments = new ArrayList<>();

        ArrayList<ReviewDetails> reviewDetails = reviewDetailsService.findBySWAndStatus(id, "done");

        if (reviewDetails.isEmpty()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        for (ReviewDetails rd: reviewDetails){
            comments.add(rd.getCommentToAuthor());
        }

        return new ResponseEntity<>(comments, HttpStatus.OK);

    }

    @RequestMapping(
            value = "/searching",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<List<ScientificWorkResult>> searchAcceptedWorks(@RequestBody List<SearchQuery> searchQueries){

        // communication with searching gateway project

        List<ScientificWorkResult> results = scientificWorkService.searchAcceptedWorks(searchQueries);

        if (results.isEmpty()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(results, HttpStatus.OK);
    }

    @RequestMapping(
            value = "/validation/tasks/{taskId}/processes/{processInstanceId}",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<String> firstValidationOfSw(@RequestBody List<FormSubmission> objects,
                                                      @PathVariable String taskId,
                                                      @PathVariable String processInstanceId) {

        HashMap<String, Object> map = HelperFunctions.mapListToObject(objects);
        Long id = Long.parseLong(map.get("swId").toString());
        map.remove("swId");
        map.remove("comment");
        map.remove("deadlineDays");

        runtimeService.setVariable(processInstanceId, "firstValidationSw", objects);

        Optional<ScientificWork> scientificWork = scientificWorkService.findById(id);
        System.out.println(scientificWork.get().getTitle() + scientificWork.get().getKeywords() + scientificWork.get().getStatus());


        formService.submitTaskForm(taskId, map);

        return new ResponseEntity<>("Everything is ok!", HttpStatus.OK);
    }

    @RequestMapping(
            value = "/editors/{chosenEditor}/to-revision",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<List<TaskSWDto>> getAcceptedScientificWorks(@PathVariable String chosenEditor){

        // Goal is to find all scientific works that need to be sent to revision and are assigned to given editor

        Optional<Editor> editor = editorService.findByUsername(chosenEditor);

        if (!editor.isPresent()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        System.out.println("Editor chosen from controller: " + chosenEditor);
        List<Task> tasks = taskService.createTaskQuery().taskAssignee(chosenEditor).taskDefinitionKey("ChoosingReviewersTask").list();
        List<TaskSWDto> taskDtos = scientificWorkService.getAssignedWorks(tasks, "revision-needed", "major-updates-done");

        return new ResponseEntity<>(taskDtos, HttpStatus.OK);
    }

    @RequestMapping(
            value = "/authors/{authorUname}/format-change",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<List<TaskSWDto>> getAuthorsWorksForChanging(@PathVariable String authorUname){

        // Goal is to find all scientific works that need to be changed in order to be accepted by editor

        Optional<Author> author = authorService.findByUsername(authorUname);

        if (!author.isPresent()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        System.out.println("Author from controller: " + authorUname);
        List<Task> tasks = taskService.createTaskQuery().taskAssignee(authorUname).taskDefinitionKey("FormatChangingTask").list();
        List<TaskSWDto> taskDtos = scientificWorkService.getWorks(tasks, "format-changing");

        return new ResponseEntity<>(taskDtos, HttpStatus.OK);
    }

    @RequestMapping(
            value = "/editors/{editorUname}/minor-major-changed",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<List<TaskSWDto>> getMinorMajorChangedWorks(@PathVariable String editorUname){


        Optional<Editor> editor = editorService.findByUsername(editorUname);

        if (!editor.isPresent()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        List<Task> tasks = taskService.createTaskQuery().taskAssignee(editorUname).taskDefinitionKey("EditorSecondDecisionTask").list();
        List<TaskSWDto> taskDtos = scientificWorkService.getWorks(tasks, "minor-updates-done");

        return new ResponseEntity<>(taskDtos, HttpStatus.OK);
    }

    @RequestMapping(
            value = "/authors/{authorUname}/from-revision",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<List<TaskSWDto>> getReviewedWorksForChanging(@PathVariable String authorUname){

        // Goal is to find all scientific works that need to be changed in order to be accepted
        // by editor and have already been reviewed

        Optional<Author> author = authorService.findByUsername(authorUname);

        if (!author.isPresent()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        System.out.println("Author from controller: " + authorUname);
        List<Task> tasks = taskService.createTaskQuery().taskAssignee(authorUname).taskDefinitionKey("ChangingWorkTask").list();
        List<TaskSWDto> taskDtos = scientificWorkService.getAssignedWorks(tasks, "minor-updates", "major-updates");

        return new ResponseEntity<>(taskDtos, HttpStatus.OK);
    }

    @RequestMapping(
            value = "/{id}/revisions",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<List<ReviewDetailsDTO>> getRevisionFormFields(@PathVariable Long id){

        List<ReviewDetailsDTO> reviewDetailsDTOS = new ArrayList<>();

        Optional<ScientificWork> scientificWork = scientificWorkService.findById(id);
        if (!scientificWork.isPresent()) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        ArrayList<ReviewDetails> reviewDetails = reviewDetailsService.findBySWAndStatus(id, "done");

        if (!reviewDetails.isEmpty()) {
            for (ReviewDetails rd: reviewDetails) {
                Optional<Reviewer> reviewer = reviewerService.findById(rd.getReviewer().getId());
                if (reviewer.isPresent()){
                    ReviewDetailsDTO dto = new ReviewDetailsDTO(rd, reviewer.get());
                    reviewDetailsDTOS.add(dto);
                }
            }
        }

        return new ResponseEntity<>(reviewDetailsDTOS, HttpStatus.OK);
    }

    @RequestMapping(
            value = "/tasks/{taskId}/author-changes",
            method = RequestMethod.POST
    )
    public ResponseEntity<String> authorChangesScientificWork(
                                                        @RequestParam(value = "file", required = false) MultipartFile file,
                                                        @RequestParam("title") String title,
                                                        @RequestParam("keywords") String keywords,
                                                        @RequestParam("abstract") String abstract_,
                                                        @RequestParam("subjectArea") String subjectArea,
                                                        @RequestParam("id") String id,
                                                        @RequestParam("answeringComments") String answeringComments,
                                                        @PathVariable String taskId) {

        ArrayList<FormSubmission> objects = new ArrayList<>();
        objects.add(new FormSubmission("answeringComments", answeringComments));


        Task task = taskService.createTaskQuery().taskId(taskId).singleResult();
        String processInstanceId = task.getProcessInstanceId();
        ArrayList<FormSubmission> object2 = new ArrayList<>(Arrays.asList(
                new FormSubmission("title", title),
                new FormSubmission("keywords", keywords),
                new FormSubmission("abstract", abstract_),
                new FormSubmission("answeringComments", answeringComments)
        ));


        runtimeService.setVariable(processInstanceId, "authorChanges", object2);

        runtimeService.setVariable(processInstanceId, "swForChange", Long.parseLong(id));

        String username = (String) runtimeService.getVariable(processInstanceId, "loggedInUser");

        String path = saveFile(file, username, title);

        HashMap<String, Object> map = HelperFunctions.mapListToObject(objects);

        map.put("pdf", path);

        formService.submitTaskForm(taskId, map);

        return new ResponseEntity<>("Everything is ok!", HttpStatus.OK);
    }

    @RequestMapping(
            value = "/editors/{editorUname}/second-decision",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<FormDetails> getAuthorChangesWorkFormFiels(@PathVariable String editorUname){

        Task task = taskService.createTaskQuery().taskAssignee(editorUname).taskDefinitionKey("EditorSecondDecisionTask").list().get(0);

        TaskFormData taskFormData = formService.getTaskFormData(task.getId());
        List<FormField> formFields = taskFormData.getFormFields();

        FormDetails formDetails = new FormDetails(task.getId(), formFields, task.getProcessInstanceId());

        return new ResponseEntity<>(formDetails, HttpStatus.OK);
    }

    @RequestMapping(
            value = "/{id}/minor-changes-details",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<MinorMajorChangesDetailsDTO> getAuthorChangesWorkFormFiels(@PathVariable Long id){

        Optional<ScientificWork> scientificWork = scientificWorkService.findById(id);
        if (!scientificWork.isPresent()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        Optional<MinorMajorChangesDetails> details = minorMajorChangesDetailsService.findByScientificWorkId(id);

        if (!details.isPresent()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        MinorMajorChangesDetailsDTO detailsDTO = new MinorMajorChangesDetailsDTO(
                details.get().getAnsweringComments(), scientificWork.get(), scientificWork.get().getAuthors());

        return new ResponseEntity<>(detailsDTO, HttpStatus.OK);
    }


    public String saveFile(MultipartFile file, String username, String title){
        String path = "pdf file";
        try {

            System.out.println("File:  " + file.getOriginalFilename());
            FileHandler fileHandler = new FileHandler();

            if (!username.equals("")){
                path = fileHandler.saveFile(file, username, title);
                System.out.println("Path to file: " + path);
            }


        } catch (Exception e){
            System.out.println("No pdf file is provided!");
            e.printStackTrace();
        }
        return path;
    }
}
