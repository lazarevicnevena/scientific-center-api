package rs.ac.uns.ftn.siit.scientificcenterapi.service.process;

import org.camunda.bpm.engine.IdentityService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.siit.scientificcenterapi.dto.AuthorDTO;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.Author;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.ScientificWork;
import rs.ac.uns.ftn.siit.scientificcenterapi.service.application.AuthorService;
import rs.ac.uns.ftn.siit.scientificcenterapi.service.application.NotificationService;
import rs.ac.uns.ftn.siit.scientificcenterapi.service.application.ScientificWorkService;

import java.util.Optional;

@Service
public class WorkRejectedService implements JavaDelegate {

    @Autowired
    IdentityService identityService;

    @Autowired
    ScientificWorkService scientificWorkService;

    @Autowired
    NotificationService notificationService;

    @Autowired
    AuthorService authorService;

    @Override
    public void execute(DelegateExecution execution) throws Exception {

        System.out.println("Entered Work Rejected Service Task");

        Long id = (Long) execution.getVariable("swId");

        Optional<ScientificWork> scientificWork = scientificWorkService.findById(id);

        AuthorDTO authorDTO = (AuthorDTO) execution.getVariable("author");
        Optional<Author> authorOption = authorService.findByUsername(authorDTO.getUsername());

        if (scientificWork.isPresent()) {
            scientificWork.get().setStatus("rejected");
            scientificWorkService.save(scientificWork.get());

            if (authorOption.isPresent()){

                String header =  "Poštovani/a " + authorOption.get().getJournalUser().getFirstName() + " " +
                        authorOption.get().getJournalUser().getLastName() + ", <br/><br/>";

                String body = header + "Vaš naučni rad pod nazivom <b><i>" + scientificWork.get().getTitle() +
                        "</i></b> je odbijen, te neće biti objavljen u časopisu!";

                notificationService.sendMessage(
                        authorOption.get().getJournalUser().getEmail(),
                        "Naučni rad odbijen",
                        body);
            }
        }
    }
}
