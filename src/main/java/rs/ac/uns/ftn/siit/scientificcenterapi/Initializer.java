package rs.ac.uns.ftn.siit.scientificcenterapi;

import org.camunda.bpm.engine.IdentityService;
import org.camunda.bpm.engine.identity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.*;
import rs.ac.uns.ftn.siit.scientificcenterapi.service.application.*;

import java.math.BigDecimal;
import java.time.LocalDate;


@Component
public class Initializer implements ApplicationRunner {

    @Autowired
    private JournalService journalService;

    @Autowired
    private JournalUserService journalUserService;

    @Autowired
    private AuthorService authorService;

    @Autowired
    private ReviewerService reviewerService;

    @Autowired
    private EditorService editorService;

    @Autowired
    private SubjectAreaService subjectAreaService;

    @Autowired
    private ScientificWorkService scientificWorkService;

    @Autowired
    Environment environment;

    @Autowired
    IdentityService identityService;

    @Autowired
    FormatInvalidDetailsService formatInvalidDetailsService;

    @Autowired
    JournalMembershipService journalMembershipService;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        System.out.println("Adding data to DB");

        addData();

    }

    private void addData() {

        User u = identityService.createUserQuery().userId("neca").singleResult();
        if (u == null) {
            User user = identityService.newUser("");
            user.setId("neca");
            user.setPassword("pereca");
            identityService.saveUser(user);
        }

        JournalUser journalUser = new JournalUser();
        journalUser.setUsername("neca");
        journalUser.setFirstName("Nevena");
        journalUser.setLastName("Lazarević");
        journalUser.setEmail("lazarevic.nevena.43@gmail.com");
        journalUser.setCity("Novi Sad");
        journalUser.setState("Srbija");
        journalUser = journalUserService.save(journalUser);

        Editor editor = new Editor();
        editor.setRank("Diplomirani inžinjer elektrotehnike i računarstva");
        editor.setJournalUser(journalUser);
        editor = editorService.save(editor);

        Journal journal = new Journal();
        journal.setIssn("123456789");
        journal.setOpenAccess(true);
        journal.setTitle("Časopis Žurnal");
        journal.setSellerId("1234");
        journal.setEditor(editor);
        journal.setPaymentOptions("bankCard|paypal|bitcoin");
        journal.setPrice(new BigDecimal(100));
        journal = journalService.save(journal);

        User u1 = identityService.createUserQuery().userId("aki").singleResult();
        if (u1 == null) {
            User user1 = identityService.newUser("");
            user1.setId("aki");
            user1.setPassword("brat");
            identityService.saveUser(user1);
        }


        JournalUser journalUser2 = new JournalUser();
        journalUser2.setUsername("aki");
        journalUser2.setFirstName("Radoš");
        journalUser2.setLastName("Aćimović");
        journalUser2.setEmail("necapereca96@hotmail.com");
        journalUser2.setCity("Novi Sad");
        journalUser2.setState("Srbija");
        journalUser2 = journalUserService.save(journalUser2);

        Reviewer reviewer = new Reviewer();
        reviewer.setJournalUser(journalUser2);
        reviewer.setRank("Diplomirani inžinjer elektrotehnike i računarstva");
        reviewer.setSubjectAreas("Matematika, Informatika");
        reviewer.getJournals().add(journal);
        reviewer = reviewerService.save(reviewer);

        User u2 = identityService.createUserQuery().userId("ana").singleResult();
        if (u2 == null) {
            User user2 = identityService.newUser("");
            user2.setId("ana");
            user2.setPassword("123");
            identityService.saveUser(user2);
        }


        JournalUser journalUser3 = new JournalUser();
        journalUser3.setUsername("ana");
        journalUser3.setFirstName("Ana");
        journalUser3.setLastName("Rudić");
        journalUser3.setEmail("ana@gmail.com");
        journalUser3.setCity("Novi Sad");
        journalUser3.setState("Srbija");
        journalUser3 = journalUserService.save(journalUser3);

        Editor editor2 = new Editor();
        editor2.setRank("Diplomirani inžinjer elektrotehnike i računarstva");
        editor2.setJournalUser(journalUser3);
        editor2 = editorService.save(editor2);

        SubjectArea subjectArea = new SubjectArea();
        subjectArea.setArea("Informacione tehnologije");
        subjectArea.setAreaEditor(editor2);
        subjectArea.setJournal(journal);
        subjectAreaService.save(subjectArea);

        SubjectArea subjectArea2 = new SubjectArea();
        subjectArea2.setArea("Matematika");
        subjectArea2.setAreaEditor(editor2);
        subjectArea2.setJournal(journal);
        subjectAreaService.save(subjectArea2);

        editor.setJournal(journal);
        editorService.save(editor);

        editor2.setJournal(journal);
        editorService.save(editor2);

        Reviewer reviewer1 = new Reviewer();
        reviewer1.setRank("Diplomirani inžinjer elektrotehnike i računarstva");
        reviewer1.setJournalUser(journalUser3);
        reviewer1.getJournals().add(journal);
        reviewer1 = reviewerService.save(reviewer1);


        User u3 = identityService.createUserQuery().userId("sreten").singleResult();
        if (u3 == null) {
            User user3 = identityService.newUser("");
            user3.setId("sreten");
            user3.setPassword("123");
            identityService.saveUser(user3);
        }


        JournalUser journalUser4 = new JournalUser();
        journalUser4.setUsername("sreten");
        journalUser4.setFirstName("Sreten");
        journalUser4.setLastName("Stokić");
        journalUser4.setEmail("lazarevic.nevena.43@gmail.com");
        journalUser4.setCity("Kopaonik");
        journalUser4.setState("Srbija");
        journalUser4 = journalUserService.save(journalUser4);

        Author author = new Author();
        author.setJournalUser(journalUser4);
        author.setJournal(journal);
        author = authorService.save(author);

        Membership membership = new Membership();
        membership.setActive(true);
        membership.setAuthor(author);
        membership.setJournal(journal);
        LocalDate localDate = LocalDate.of(2018, 10, 15);
        membership.setUntil(localDate);
        journalMembershipService.save(membership);

        ScientificWork scientificWork = new ScientificWork();
        scientificWork.getAuthors().add(author);
        scientificWork.setJournal(journal);
        scientificWork.setKeywords("it,tehnologija");
        scientificWork.setTitle("Prikupljanje logova");
        scientificWork.setSubjectArea("Informatika");
        scientificWork.setShortAbstract("Apstrakt od tri rijeci");
        scientificWork.setStatus("accepted");
        scientificWork.setDoi("10.1575/1237894563");
        scientificWork = scientificWorkService.save(scientificWork);

        User u5 = identityService.createUserQuery().userId("kenny").singleResult();
        if (u5 == null) {
            User user5 = identityService.newUser("");
            user5.setId("kenny");
            user5.setPassword("123");
            identityService.saveUser(user5);
        }


        JournalUser journalUser5 = new JournalUser();
        journalUser5.setUsername("kenny");
        journalUser5.setFirstName("Nemanja");
        journalUser5.setLastName("Lazarević");
        journalUser5.setEmail("necapereca96@hotmail.com");
        journalUser5.setCity("Novi Sad");
        journalUser5.setState("Srbija");
        journalUser5 = journalUserService.save(journalUser5);

        Reviewer reviewer2 = new Reviewer();
        reviewer2.setJournalUser(journalUser5);
        reviewer2.setRank("Diplomirani inžinjer elektrotehnike i računarstva");
        reviewer2.setSubjectAreas("Matematika, Informatika");
        reviewer2.getJournals().add(journal);
        reviewerService.save(reviewer2);

        User u6 = identityService.createUserQuery().userId("miki").singleResult();
        if (u6 == null) {
            User user6 = identityService.newUser("");
            user6.setId("miki");
            user6.setPassword("123");
            identityService.saveUser(user6);
        }


        JournalUser journalUser6 = new JournalUser();
        journalUser6.setUsername("miki");
        journalUser6.setFirstName("Milan");
        journalUser6.setLastName("Milanović");
        journalUser6.setEmail("miki@gmail.com");
        journalUser6.setCity("Banja Luka");
        journalUser6.setState("BIH");
        journalUser6 = journalUserService.save(journalUser6);

        Reviewer reviewer3 = new Reviewer();
        reviewer3.setJournalUser(journalUser6);
        reviewer3.setRank("Diplomirani inžinjer elektrotehnike i računarstva");
        reviewer3.setSubjectAreas("Matematika");
        reviewer3.getJournals().add(journal);
        reviewerService.save(reviewer3);

        /*
        FormatInvalidDetails formatInvalidDetails = new FormatInvalidDetails();
        formatInvalidDetails.setComment("Ovo je komm");
        formatInvalidDetails.setScientificWork(scientificWork);
        formatInvalidDetailsService.save(formatInvalidDetails);
        */
    }
}