package rs.ac.uns.ftn.siit.scientificcenterapi.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.Author;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.ScientificWork;

import java.util.ArrayList;
import java.util.Optional;


public interface ScientificWorkRepository extends JpaRepository<ScientificWork, Long> {
    ArrayList<ScientificWork> findByAuthors(ArrayList<Author> authors);
    ArrayList<ScientificWork> findByJournal_Issn(String issn);
    Page<ScientificWork> findAll(Pageable pageable);
    Page<ScientificWork> findAllByStatus(Pageable pageable, String status);
    Page<ScientificWork> findAllByJournal_Id(Pageable pageable, Long id);
    ArrayList<ScientificWork> findByStatus(String status);

    Optional<ScientificWork> findById(Long id);
}
