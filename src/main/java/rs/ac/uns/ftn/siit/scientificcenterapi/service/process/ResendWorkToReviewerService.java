package rs.ac.uns.ftn.siit.scientificcenterapi.service.process;

import org.camunda.bpm.engine.IdentityService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.FormSubmission;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.MinorMajorChangesDetails;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.ReviewDetails;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.ScientificWork;
import rs.ac.uns.ftn.siit.scientificcenterapi.service.application.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ResendWorkToReviewerService implements JavaDelegate {

    @Autowired
    MinorMajorChangesDetailsService minorMajorChangesDetailsService;

    @Autowired
    ScientificWorkService scientificWorkService;

    @Autowired
    NotificationService notificationService;

    @Autowired
    ReviewerService reviewerService;

    @Autowired
    ReviewDetailsService reviewDetailsService;

    @Override
    public void execute(DelegateExecution execution) throws Exception {

        System.out.println("Entered Resend Work To Reviewer Service Task");

        List<FormSubmission> objects = (List<FormSubmission>)execution.getVariable("authorChanges");
        String title = "";
        String abstract_ = "";
        String keywords = "";
        String answComments = "";
        for (FormSubmission formField : objects) {
            if(formField.getFieldId().equals("title")) {
                title = formField.getFieldValue();
            }
            if(formField.getFieldId().equals("abstract")) {
                abstract_ = formField.getFieldValue();
            }
            if(formField.getFieldId().equals("keywords")) {
                keywords = formField.getFieldValue();
            }
            if(formField.getFieldId().equals("answeringComments")) {
                answComments = formField.getFieldValue();
            }
        }

        Long id = (Long) execution.getVariable("swForChange");

        Optional<ScientificWork> scientificWork = scientificWorkService.findById(id);

        if (scientificWork.isPresent()){
            scientificWork.get().setKeywords(keywords);
            scientificWork.get().setShortAbstract(abstract_);
            scientificWork.get().setStatus("major-updates-done");
            ScientificWork sw = scientificWorkService.save(scientificWork.get());

            Optional<MinorMajorChangesDetails> minorMajorChangesDetails =
                    minorMajorChangesDetailsService.findByScientificWorkId(sw.getId());
            if (minorMajorChangesDetails.isPresent()){
                MinorMajorChangesDetails majorDetails = minorMajorChangesDetails.get();
                majorDetails.setAnsweringComments(answComments);
                minorMajorChangesDetailsService.save(majorDetails);
            } else {
                MinorMajorChangesDetails details = new MinorMajorChangesDetails();
                details.setScientificWork(sw);
                details.setAnsweringComments(answComments);
                minorMajorChangesDetailsService.save(details);
            }

            System.out.println("Author changes major changes!");

            ArrayList<ReviewDetails> reviewDetails = reviewDetailsService.findBySWAndStatus(sw.getId(), "done");

            if (!reviewDetails.isEmpty()){
                for (ReviewDetails rd: reviewDetails){
                    String header =  "Poštovani/a " + rd.getReviewer().getJournalUser().getFirstName() + " " +
                            rd.getReviewer().getJournalUser().getLastName() + ", <br/><br/>";

                    String body = header + "Naučni rad pod nazivom <b><i>" + sw.getTitle() +
                            "</i></b> je vraćen sa prepravljanja, te Vas molimo da ponovo recenzirate pomenuti rad!";

                    notificationService.sendMessage(
                            rd.getReviewer().getJournalUser().getEmail(),
                            "Naučni rad prepravljen",
                            body);
                }
            }
        }

        execution.removeVariable("authorChanges");
        execution.removeVariable("swForChange");

    }
}
