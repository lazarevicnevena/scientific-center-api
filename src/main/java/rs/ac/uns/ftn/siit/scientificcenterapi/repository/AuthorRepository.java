package rs.ac.uns.ftn.siit.scientificcenterapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.Author;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.ScientificWork;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


public interface AuthorRepository extends JpaRepository<Author, Long> {
    ArrayList<Author> findByScientificWorks(List<ScientificWork> scientificWorks);
    ArrayList<Author> findByJournal_Id(Long id);

    Optional<Author> findByJournalUser_Username(String username);
    Optional<Author> findByJournalUser_Email(String email);
}
