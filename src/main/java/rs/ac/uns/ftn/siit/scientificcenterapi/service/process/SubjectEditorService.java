package rs.ac.uns.ftn.siit.scientificcenterapi.service.process;

import org.camunda.bpm.engine.IdentityService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.siit.scientificcenterapi.dto.JournalDTO;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.Editor;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.Journal;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.ScientificWork;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.SubjectArea;
import rs.ac.uns.ftn.siit.scientificcenterapi.service.application.*;

import java.util.Optional;

@Service
public class SubjectEditorService implements JavaDelegate {

    @Autowired
    IdentityService identityService;

    @Autowired
    JournalService journalService;

    @Autowired
    SubjectAreaService subjectAreaService;

    @Autowired
    EditorService editorService;

    @Autowired
    ScientificWorkService scientificWorkService;

    @Autowired
    NotificationService notificationService;

    @Override
    public void execute(DelegateExecution execution) throws Exception {

        System.out.println("Entered Subject Editor Service Task");

        JournalDTO journalDTO = (JournalDTO) execution.getVariable("journal");
        System.out.println("journal DTO From service: " + journalDTO.toString());
        Optional<Journal> journal = journalService.findByISSN(journalDTO.getIssn());

        Long id = (Long) execution.getVariable("swId");

        Optional<ScientificWork> scientificWork = scientificWorkService.findById(id);

        scientificWork.get().setStatus("revision-needed");
        scientificWorkService.save(scientificWork.get());

        if (journal.isPresent()){
            String area = scientificWork.get().getSubjectArea();
            Optional<SubjectArea> subjectArea = subjectAreaService.findByJournalIssnAndArea(journal.get().getIssn(), area);
            if (subjectArea.isPresent()){
                execution.setVariable("chosenEditor", subjectArea.get().getAreaEditor().getJournalUser().getUsername());
            } else {
                execution.setVariable("chosenEditor", journal.get().getEditor().getJournalUser().getUsername());
            }
        }

        System.out.println("Editor chosen: " + execution.getVariable("chosenEditor"));

        String chosenEditor = (String) execution.getVariable("chosenEditor");


        Optional<Editor> editor = editorService.findByUsername(chosenEditor);

        if (editor.isPresent()){

            String header =  "Poštovani/a " + editor.get().getJournalUser().getFirstName() + " " +
                    editor.get().getJournalUser().getLastName() + ", <br/><br/>";

            String body = header + "Odabrani ste za urednika naučne oblasti za koju je rad pod nazivom <b><i>" +
                    scientificWork.get().getTitle() +
                    "</i></b> prijavljen. Na vama je da odaberete najmanje dva recenzenta!";

            notificationService.sendMessage(
                    editor.get().getJournalUser().getEmail(),
                    "Dodijeljeni ste kao urednik naučne oblasti",
                    body);
        }
    }
}
