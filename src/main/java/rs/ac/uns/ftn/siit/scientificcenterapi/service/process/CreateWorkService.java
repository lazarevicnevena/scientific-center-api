package rs.ac.uns.ftn.siit.scientificcenterapi.service.process;

import org.camunda.bpm.engine.IdentityService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.siit.scientificcenterapi.dto.AuthorDTO;
import rs.ac.uns.ftn.siit.scientificcenterapi.dto.JournalDTO;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.*;
import rs.ac.uns.ftn.siit.scientificcenterapi.service.application.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CreateWorkService implements JavaDelegate{

	@Autowired
	IdentityService identityService;

	@Autowired
    ScientificWorkService scientificWorkService;

	@Autowired
    AuthorService authorService;

	@Autowired
    JournalService journalService;

	@Autowired
    JournalUserService journalUserService;

	@Autowired
    NotificationService notificationService;
	
	@Override
	public void execute(DelegateExecution execution) throws Exception {

		System.out.println("Entered Create Scientific Work Service Task");

        List<FormSubmission> scientificWorks = (List<FormSubmission>)execution.getVariable("scientificWorkSubmission");
        System.out.println(scientificWorks);
        String title = "";
        String authors = "";
        String keywords = "";
        String shortAbstract = "";
        String subjectArea = "";

        for (FormSubmission formField : scientificWorks) {
            if(formField.getFieldId().equals("title")) {
                title = formField.getFieldValue();
            }
            if(formField.getFieldId().equals("keywords")) {
                keywords = formField.getFieldValue();
            }
            if(formField.getFieldId().equals("abstract")) {
                shortAbstract = formField.getFieldValue();
            }
            if(formField.getFieldId().equals("subjectArea")) {
                subjectArea = formField.getFieldValue();
            }
            if(formField.getFieldId().equals("authors")) {
                authors = formField.getFieldValue();
            }
        }

        JournalDTO journalDTO = (JournalDTO) execution.getVariable("journal");

        Optional<Journal> journal = journalService.findByISSN(journalDTO.getIssn());

        ScientificWork sw = null;

        Long idSW = (Long) execution.getVariable("swForChange");

        AuthorDTO authorDTO = (AuthorDTO) execution.getVariable("author");
        Optional<Author> authorMain = authorService.findByUsername(authorDTO.getUsername());

        String uname = authorMain.get().getJournalUser().getUsername();
        execution.setVariable("authorUsername", uname);
        System.out.println("Author uname: " + uname);

        if (idSW == 0L) {
            Boolean flag = false;
            String[] tokens = new String[10];
            if (authors != null && authors.contains("|")){
                tokens = authors.split("\\|");
                flag = true;
            }


            List<Author> authorsList = new ArrayList<>();
            authorsList.add(authorMain.get());
            if (flag){
                for (int i=0; i < tokens.length; i++){
                    String[] tokensAuthor = tokens[i].split("-");
                    System.out.println("TOKENS: " + tokensAuthor);
                    Optional<Author> author = authorService.findByEmail(tokensAuthor[2]);
                    if (author.isPresent()){
                        authorsList.add(author.get());
                    }else {
                        Author author1 = new Author();
                        JournalUser journalUser = new JournalUser();
                        journalUser.setFirstName(tokensAuthor[0]);
                        journalUser.setLastName(tokensAuthor[1]);
                        journalUser.setEmail(tokensAuthor[2]);
                        journalUser.setCity(tokensAuthor[3]);
                        journalUser.setState(tokensAuthor[4]);
                        journalUser = journalUserService.save(journalUser);
                        author1.setJournalUser(journalUser);
                        author1.setJournal(journal.get());
                        author1 = authorService.save(author1);
                        authorsList.add(author1);

                    }

                }
            }

            // check if username is taken

            ScientificWork scientificWork = new ScientificWork();
            scientificWork.setTitle(title);
            scientificWork.setKeywords(keywords);
            scientificWork.setSubjectArea(subjectArea);
            scientificWork.setShortAbstract(shortAbstract);
            scientificWork.setAuthors(authorsList);
            scientificWork.setJournal(journal.get());
            scientificWork.setStatus("submitted");

            sw = scientificWorkService.save(scientificWork);
        } else {
            Optional<ScientificWork> scientificWork = scientificWorkService.findById(idSW);
            if (scientificWork.isPresent()){
                scientificWork.get().setStatus("format-changed");
                scientificWork.get().setShortAbstract(shortAbstract);
                scientificWork.get().setKeywords(keywords);
                scientificWork.get().setSubjectArea(subjectArea);

                sw = scientificWorkService.save(scientificWork.get());
            }
        }


        System.out.println("Naucni rad: " + sw.getKeywords() + " naziv: " + sw.getTitle() + " status: " + sw.getStatus());

        execution.setVariable("scientificWork", sw);

        String editor = (String) execution.getVariable("editor");

        String authorEmail = "";
        String editorEmail = "";
        if (authorMain.isPresent()){
            authorEmail = authorMain.get().getJournalUser().getEmail();
        }

        Optional<JournalUser> editorUser = journalUserService.findByUsername(editor);
        if (editorUser.isPresent()){
            editorEmail = editorUser.get().getEmail();
        }

        System.out.println("Editor: " + editorEmail + " & Author: " + authorEmail);

        execution.removeVariable("scientificWorkSubmission");
        execution.removeVariable("swForChange");

        String header =  "Poštovani/a " + authorMain.get().getJournalUser().getFirstName() + " " +
                authorMain.get().getJournalUser().getLastName() + ", <br/>";

        String bodyAuthor = header +
                "Vaš naučni rad pod nazivom <b><i>" + sw.getTitle() +
                "</i></b> je prihvaćen i poslat uredništvu časopisa na razmatranje!";

        notificationService.sendMessage(
                authorEmail,
                "Naučni rad primljen",
                bodyAuthor);

        String headerEditor =  "Poštovani/a " + editorUser.get().getFirstName() + " " +
                editorUser.get().getLastName() + ", <br/><br/>";

        String bodyEditor = headerEditor + "Naučni rad pod nazivom <b><i>" + sw.getTitle() +
                "</i></b> je pristigao u sistem i možete ga pronaći među pristiglim radovima!";

        notificationService.sendMessage(
                editorEmail,
                "Novi naučni rad u sistemu",
                bodyEditor);
	}
}
