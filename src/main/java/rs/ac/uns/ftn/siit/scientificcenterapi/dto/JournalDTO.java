package rs.ac.uns.ftn.siit.scientificcenterapi.dto;

import lombok.*;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.Editor;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.Journal;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.SubjectArea;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class JournalDTO implements Serializable {

    private String title;
    private String issn;
    private boolean openAccess;
    private BigDecimal price;
    private String sellerId;
    private EditorDTO editor;
    private List<SubjectAreaDTO> subjectAreas = new ArrayList<>();


    public JournalDTO(Journal journal, List<SubjectArea> subjectAreas){
        this.title = journal.getTitle();
        this.issn = journal.getIssn();
        this.openAccess = journal.isOpenAccess();
        if (journal.getEditor() != null){
            this.editor = new EditorDTO(journal.getEditor());
        }
        for (SubjectArea sa: subjectAreas) {
            this.getSubjectAreas().add(new SubjectAreaDTO(sa));
        }
    }
}
