package rs.ac.uns.ftn.siit.scientificcenterapi.service.process;

import org.camunda.bpm.engine.IdentityService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.FormSubmission;

import java.util.List;

@Service
public class CheckWorkValidService implements JavaDelegate{

	@Autowired
	IdentityService identityService;
	
	@Override
	public void execute(DelegateExecution execution) throws Exception {

        System.out.println("Entered Check Work Valid Service Task");

        List<FormSubmission> validation = (List<FormSubmission>)execution.getVariable("firstValidationSw");

        Boolean topicAcceptable = false;

        Boolean formatAcceptable = false;

        Long swId = 0L;

        String comment = "";

        Integer deadlineDays = 0;

        for (FormSubmission formField : validation) {
            if (formField.getFieldId().equals("topicAcceptable")) {
                topicAcceptable = Boolean.parseBoolean(formField.getFieldValue());
            }
            if (formField.getFieldId().equals("formatAcceptable")) {
                formatAcceptable = Boolean.parseBoolean(formField.getFieldValue());
            }
            if (formField.getFieldId().equals("swId")) {
                swId = Long.parseLong(formField.getFieldValue());
            }
            if (formField.getFieldId().equals("comment")) {
                comment = formField.getFieldValue();
            }
            if (formField.getFieldId().equals("deadlineDays")) {
                deadlineDays = Integer.parseInt(formField.getFieldValue());
            }
        }

        execution.setVariable("topicAcceptable", topicAcceptable);
        execution.setVariable("formatAcceptable", formatAcceptable);

        execution.setVariable("swId", swId);

        if (topicAcceptable && !formatAcceptable){
            execution.setVariable("deadlineDays", deadlineDays);
            execution.setVariable("comment", comment);
        }

        execution.removeVariable("firstValidationSw");
	}
}
