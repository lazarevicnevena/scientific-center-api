package rs.ac.uns.ftn.siit.scientificcenterapi.service.process;

import org.camunda.bpm.engine.IdentityService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.camunda.bpm.engine.identity.User;
import org.camunda.bpm.engine.identity.UserQuery;
import org.camunda.bpm.engine.impl.identity.Authentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.FormSubmission;

import java.util.List;

@Service
public class LoginService implements JavaDelegate{

	@Autowired
	IdentityService identityService;
	
	@Override
	public void execute(DelegateExecution execution) throws Exception {

        System.out.println("Entered Login Service Task");

		List<FormSubmission> login = (List<FormSubmission>)execution.getVariable("login");
		String username = "";
		String pwd = "";
		for (FormSubmission formField : login) {
			if(formField.getFieldId().equals("password")) {
				pwd = formField.getFieldValue();
			}
			if(formField.getFieldId().equals("username")) {
				username = formField.getFieldValue();
			}
		}
		// check if user credentials are ok
        Boolean b = identityService.checkPassword(username, pwd);
		execution.setVariable("isLoggedIn", b);
		execution.setVariable("loggedInUser", username);

		execution.removeVariable("login");

	}
}
