package rs.ac.uns.ftn.siit.scientificcenterapi.service.process;

import jdk.nashorn.internal.runtime.options.Option;
import org.camunda.bpm.engine.IdentityService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.siit.scientificcenterapi.dto.AuthorDTO;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.Author;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.ScientificWork;
import rs.ac.uns.ftn.siit.scientificcenterapi.service.application.AuthorService;
import rs.ac.uns.ftn.siit.scientificcenterapi.service.application.NotificationService;
import rs.ac.uns.ftn.siit.scientificcenterapi.service.application.ScientificWorkService;

import java.util.Optional;

@Service
public class WorkTechnicallyRejectedService implements JavaDelegate {

    @Autowired
    IdentityService identityService;

    @Autowired
    AuthorService authorService;

    @Autowired
    ScientificWorkService scientificWorkService;

    @Autowired
    NotificationService notificationService;

    @Override
    public void execute(DelegateExecution execution) throws Exception {

        System.out.println("Entered Work Technically Rejected Service");

        AuthorDTO authorDTO = (AuthorDTO) execution.getVariable("author");

        Long id = (Long) execution.getVariable("swId");

        Optional<Author> authorOption = authorService.findByUsername(authorDTO.getUsername());

        String authorEmail = "";

        if (authorOption.isPresent()){
            authorEmail = authorOption.get().getJournalUser().getEmail();
            Optional<ScientificWork> scientificWork = scientificWorkService.findById(id);

            if (scientificWork.isPresent()){
                scientificWork.get().setStatus("rejected");
                scientificWorkService.save(scientificWork.get());

                String header =  "Poštovani/a " + authorOption.get().getJournalUser().getFirstName() + " " +
                        authorOption.get().getJournalUser().getLastName() + ", <br/><br/>";

                String body = header + "Vaš naučni rad pod nazivom <b><i>" + scientificWork.get().getTitle() +
                        "</i></b> je odbijen iz tehničkih razloga." +
                        "<br/> Niste prepravili format rada u zadatom roku i iz tog razloga je rad odbijen!";

                notificationService.sendMessage(
                        authorEmail,
                        "Naučni rad odbijen iz tehničkih razloga",
                        body);
            }
        }
    }
}
