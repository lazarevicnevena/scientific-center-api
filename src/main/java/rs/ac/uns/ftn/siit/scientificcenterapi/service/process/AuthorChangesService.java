package rs.ac.uns.ftn.siit.scientificcenterapi.service.process;

import org.camunda.bpm.engine.IdentityService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.siit.scientificcenterapi.dto.AuthorDTO;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.Author;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.Editor;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.ScientificWork;
import rs.ac.uns.ftn.siit.scientificcenterapi.service.application.AuthorService;
import rs.ac.uns.ftn.siit.scientificcenterapi.service.application.NotificationService;
import rs.ac.uns.ftn.siit.scientificcenterapi.service.application.ScientificWorkService;

import java.util.Optional;

@Service
public class AuthorChangesService implements JavaDelegate {

    @Autowired
    IdentityService identityService;

    @Autowired
    ScientificWorkService scientificWorkService;

    @Autowired
    AuthorService authorService;

    @Autowired
    NotificationService notificationService;

    @Override
    public void execute(DelegateExecution execution) throws Exception {

        System.out.println("Entered Author Changes Scientific Work Service Task");

        String decision = (String) execution.getVariable("decision");

        Long id = (Long) execution.getVariable("swId");

        Optional<ScientificWork> scientificWork = scientificWorkService.findById(id);

        if (scientificWork.isPresent()) {
            if (decision.equals("minorUpdates")) {
                scientificWork.get().setStatus("minor-updates");
            } else if (decision.equals("majorUpdates")) {
                scientificWork.get().setStatus("major-updates");
            }
            scientificWorkService.save(scientificWork.get());

            AuthorDTO authorDTO = (AuthorDTO) execution.getVariable("author");
            Optional<Author> authorOption = authorService.findByUsername(authorDTO.getUsername());

            if (authorOption.isPresent()){
                String header =  "Poštovani/a " + authorOption.get().getJournalUser().getFirstName() + " " +
                        authorOption.get().getJournalUser().getLastName() + ", <br/><br/>";

                String body = header + "Vaš naučni rad pod nazivom <b><i>" + scientificWork.get().getTitle() +
                        "</i></b> još nije prihvaćen. Naime postoje neke zamjerke koje trebate prepraviti!";

                notificationService.sendMessage(
                        authorOption.get().getJournalUser().getEmail(),
                        "Prepravke za naučni rad",
                        body);

            }
        }

    }
}
