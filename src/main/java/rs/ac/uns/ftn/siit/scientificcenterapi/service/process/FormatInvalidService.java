package rs.ac.uns.ftn.siit.scientificcenterapi.service.process;

import org.camunda.bpm.engine.IdentityService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.siit.scientificcenterapi.dto.AuthorDTO;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.Author;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.FormatInvalidDetails;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.ScientificWork;
import rs.ac.uns.ftn.siit.scientificcenterapi.service.application.AuthorService;
import rs.ac.uns.ftn.siit.scientificcenterapi.service.application.FormatInvalidDetailsService;
import rs.ac.uns.ftn.siit.scientificcenterapi.service.application.NotificationService;
import rs.ac.uns.ftn.siit.scientificcenterapi.service.application.ScientificWorkService;

import java.util.Optional;

@Service
public class FormatInvalidService implements JavaDelegate {

    @Autowired
    IdentityService identityService;

    @Autowired
    ScientificWorkService scientificWorkService;

    @Autowired
    FormatInvalidDetailsService formatInvalidDetailsService;

    @Autowired
    AuthorService authorService;

    @Autowired
    NotificationService notificationService;

    @Override
    public void execute(DelegateExecution execution) throws Exception {

        System.out.println("Entered Format Invalid Service Task");

        Long id = (Long) execution.getVariable("swId");

        String comment = (String) execution.getVariable("comment");
        Integer deadlineDays = (Integer) execution.getVariable("deadlineDays");

        Optional<ScientificWork> scientificWork = scientificWorkService.findById(id);

        if (scientificWork.isPresent()){
            scientificWork.get().setStatus("format-changing");
            ScientificWork sw = scientificWorkService.save(scientificWork.get());

            Optional<FormatInvalidDetails> fiD = formatInvalidDetailsService.findByScientificWorkId(id);
            if (fiD.isPresent()){
                fiD.get().setComment(comment);
                formatInvalidDetailsService.save(fiD.get());
            }else {
                FormatInvalidDetails formatInvalidDetails = new FormatInvalidDetails();
                formatInvalidDetails.setScientificWork(sw);
                formatInvalidDetails.setComment(comment);

                formatInvalidDetailsService.save(formatInvalidDetails);
            }

            String deadline = "P" + deadlineDays.toString() + "D";
            execution.setVariable("deadline", deadline);

            AuthorDTO authorDTO = (AuthorDTO) execution.getVariable("author");
            Optional<Author> authorMain = authorService.findByUsername(authorDTO.getUsername());

            if (authorMain.isPresent()){
                String header =  "Poštovani/a " + authorMain.get().getJournalUser().getFirstName() + " " +
                        authorMain.get().getJournalUser().getLastName() + ", <br/><br/>";

                String body = header + "Vaš naučni rad pod nazivom <b><i>" + scientificWork.get().getTitle() +
                        "</i></b> je vraćen na ispravljanje iz razloga što nije formatiran u skladu sa zahtjevima." +
                        "<br/> Komentar urednika o neprihvatljivom formatu <i>" + comment +
                        "</i> <br/> Na raspolaganju imate <b>" + deadlineDays.toString() +
                        "</b> dana za prepravku, u suprotnom će rad biti odbijen!";

                notificationService.sendMessage(
                        authorMain.get().getJournalUser().getEmail(),
                        "Format rada neprihvatljiv",
                        body);

            }
        }

        execution.removeVariable("comment");
        execution.removeVariable("deadlineDays");

    }
}
