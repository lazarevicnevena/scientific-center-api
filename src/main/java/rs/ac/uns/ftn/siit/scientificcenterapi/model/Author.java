package rs.ac.uns.ftn.siit.scientificcenterapi.model;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
public class Author implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    private JournalUser journalUser;

    @ManyToOne
    private Journal journal;

    @ManyToMany
    private List<ScientificWork> scientificWorks = new ArrayList<>();
}
