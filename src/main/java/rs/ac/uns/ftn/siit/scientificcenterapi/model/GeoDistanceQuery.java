package rs.ac.uns.ftn.siit.scientificcenterapi.model;

import lombok.*;
import rs.ac.uns.ftn.siit.scientificcenterapi.dto.indexing.IndexingAuthorDTO;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class GeoDistanceQuery {

    private Integer distance;

    private List<IndexingAuthorDTO> authors = new ArrayList<>();


}
