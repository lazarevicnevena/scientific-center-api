package rs.ac.uns.ftn.siit.scientificcenterapi.service.process;

import org.camunda.bpm.engine.IdentityService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.FormSubmission;

import java.util.List;

public class CheckRegistrationService implements JavaDelegate {

    @Autowired
    IdentityService identityService;

    @Override
    public void execute(DelegateExecution execution) throws Exception {
        FormSubmission formSubmission = (FormSubmission)execution.getVariable("registrationNeeded");

        Boolean flag = false;
        if(formSubmission.getFieldValue().equals("true")) {
            flag = true;
        }

        execution.setVariable("isRegistered", flag);

        execution.removeVariable("registrationNeeded");
    }
}
