package rs.ac.uns.ftn.siit.scientificcenterapi.service.process;

import org.camunda.bpm.engine.IdentityService;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.Membership;
import rs.ac.uns.ftn.siit.scientificcenterapi.service.application.AuthorService;
import rs.ac.uns.ftn.siit.scientificcenterapi.service.application.JournalMembershipService;

import java.time.LocalDate;
import java.util.Optional;

@Service
public class MembershipService implements JavaDelegate {

    @Autowired
    IdentityService identityService;

    @Autowired
    JournalMembershipService journalMembershipService;

    @Autowired
    AuthorService authorService;

    @Autowired
    RuntimeService runtimeService;

    @Override
    public void execute(DelegateExecution execution) throws Exception {

        System.out.println("Entered Membership Service Task");

        String username = (String) execution.getVariable("loggedInUser");

        Optional<Membership> membership = journalMembershipService.findByAuthorUsername(username);

        Boolean flag = false;

        if (membership.isPresent()){
            LocalDate localDate = LocalDate.now();
            if (membership.get().getActive()){
                if (membership.get().getUntil().isAfter(localDate)){
                    flag = true;
                }
            }
        }

        execution.setVariable("membershipPaid", flag);

    }
}
