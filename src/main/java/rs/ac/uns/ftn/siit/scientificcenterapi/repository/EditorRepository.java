package rs.ac.uns.ftn.siit.scientificcenterapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.Editor;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.Journal;

import java.util.ArrayList;
import java.util.Optional;

public interface EditorRepository extends JpaRepository<Editor, Long> {
    ArrayList<Editor> findByJournal_Id(Long id);
    Optional<Editor> findByJournalUser_Username(String username);
}
