package rs.ac.uns.ftn.siit.scientificcenterapi.service.application;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.Journal;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.SubjectArea;
import rs.ac.uns.ftn.siit.scientificcenterapi.repository.SubjectAreaRepository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

@Service
public class SubjectAreaService {

    @Autowired
    private SubjectAreaRepository subjectAreaRepository;

    public SubjectArea save(SubjectArea subjectArea){
        return subjectAreaRepository.save(subjectArea);
    }

    public Optional<SubjectArea> findById(Long id) {
        return subjectAreaRepository.findById(id);
    }

    public void deleteById(Long id){
        subjectAreaRepository.deleteById(id);
    }

    public Collection<SubjectArea> findAll(){
        return subjectAreaRepository.findAll();
    }

    public ArrayList<SubjectArea> findByJournal(Long id){
        return subjectAreaRepository.findByJournal_Id(id);
    }

    public Optional<SubjectArea> findByJournalIssnAndArea(String issn, String area){
        return subjectAreaRepository.findByJournal_IssnAndAndArea(issn, area);
    }
}
