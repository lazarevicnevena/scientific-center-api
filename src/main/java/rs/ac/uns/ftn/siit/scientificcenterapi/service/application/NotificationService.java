package rs.ac.uns.ftn.siit.scientificcenterapi.service.application;

import java.util.Properties;
import java.util.UUID;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;


@Service
public class NotificationService {

    private JavaMailSender javaMailSender;

    @Autowired
    public NotificationService(JavaMailSender javaMailsender){
        this.javaMailSender = javaMailsender;
    }


    public void sendMessage(String to, String subject, String body) throws MailException, MessagingException {

        Properties properties = System.getProperties();
        Session session = Session.getDefaultInstance(properties);


        MimeMessage msg = new MimeMessage(session);
        msg.setSubject(subject);
        msg.addRecipient(Message.RecipientType.TO, new InternetAddress(to));


        MimeBodyPart bodyPart = new MimeBodyPart();
        bodyPart.setText(body,  "UTF-8", "html");


        Multipart multi = new MimeMultipart();
        multi.addBodyPart(bodyPart);

        msg.setContent(multi);

        javaMailSender.send((MimeMessage) msg);

        System.out.println("Email sent.");
    }
}
