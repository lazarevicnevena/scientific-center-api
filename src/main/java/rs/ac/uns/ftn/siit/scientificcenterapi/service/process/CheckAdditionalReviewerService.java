package rs.ac.uns.ftn.siit.scientificcenterapi.service.process;

import org.camunda.bpm.engine.IdentityService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.siit.scientificcenterapi.dto.ReviewerDTO;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.ReviewDetails;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.Reviewer;
import rs.ac.uns.ftn.siit.scientificcenterapi.model.ScientificWork;
import rs.ac.uns.ftn.siit.scientificcenterapi.service.application.ReviewDetailsService;
import rs.ac.uns.ftn.siit.scientificcenterapi.service.application.ReviewerService;
import rs.ac.uns.ftn.siit.scientificcenterapi.service.application.ScientificWorkService;

import java.util.List;
import java.util.Optional;

@Service
public class CheckAdditionalReviewerService implements JavaDelegate {

    @Autowired
    IdentityService identityService;

    @Autowired
    ScientificWorkService scientificWorkService;

    @Autowired
    ReviewDetailsService reviewDetailsService;

    @Autowired
    ReviewerService reviewerService;

    @Override
    public void execute(DelegateExecution execution) throws Exception {

        System.out.println("Entered Check Additional Reviewer Service");

        Boolean additional = (Boolean) execution.getVariable("additionalRevision");

        Long id = (Long) execution.getVariable("swId");

        Optional<ScientificWork> scientificWork = scientificWorkService.findById(id);

        if (additional){
            scientificWork.get().setStatus("additional-revision-needed");
        } else{
            scientificWork.get().setStatus("reviewed");
            Long days = (Long) execution.getVariable("days");
            String dueTime = "P" + days.toString() + "D";
            System.out.println("DUE TIME: " + dueTime);
            execution.setVariable("dueTime", dueTime);
            execution.removeVariable("days");
        }

        scientificWorkService.save(scientificWork.get());

    }
}
